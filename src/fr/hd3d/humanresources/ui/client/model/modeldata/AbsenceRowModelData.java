package fr.hd3d.humanresources.ui.client.model.modeldata;

import java.util.Date;
import java.util.Map;

import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.client.PersonNameUtils;
import fr.hd3d.common.ui.client.modeldata.NameModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


/**
 * AbsenceRowModelData is used to represent absence lines in the human resources application. It contains a person and
 * several couple key-values where the key is a date representation (yyyy-mm-dd) and the value a presence code, A for
 * absence, P for presence and E for event.
 * 
 * @author HD3D
 */
public class AbsenceRowModelData extends NameModelData
{
    /** Char representation for presence. */
    public static final char PRESENCE = new Character('P');
    /** Char representation for absence. */
    public static final char ABSENCE = new Character('A');
    /** Char representation for week-end. */
    public static final char WEEKEND = new Character('W');
    /** Char representation for event. */
    public static final char EVENT = new Character('E');

    /** Automatically generated serial ID */
    private static final long serialVersionUID = -5936580785618368754L;

    /** The person concerned by the absence. */
    private PersonModelData person;

    /**
     * Default constructor, set a person in the model data (its full name) with no presence informations.
     * 
     * @param person
     *            The person concerned by the absence.
     */
    public AbsenceRowModelData(PersonModelData person)
    {
        super(PersonNameUtils.getFullName(person.getLastName(), person.getFirstName(), person.getLogin()));

        this.person = person;
    }

    /**
     * @return The person concerned by the absence.
     */
    public PersonModelData getPerson()
    {
        return person;
    }

    /**
     * Set the person concerned by the absence.
     * 
     * @param person
     *            The person concerned by the absence.
     */
    public void setPerson(PersonModelData person)
    {
        this.person = person;
    }

    /**
     * Create a week-end key-value (date-W) for the date given in parameter.
     * 
     * @param date
     *            The date on which set an absence.
     */
    public void addWeekend(DateWrapper date)
    {
        String key = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
        this.set(key, WEEKEND);
    }

    /**
     * Create a presence key-value (date-P) for the date given in parameter.
     * 
     * @param date
     *            The date on which set an absence.
     */
    public void addPresence(DateWrapper date)
    {
        String key = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
        this.set(key, PRESENCE);
    }

    /**
     * Create a presence key-value (date-P) for the all dates between <i>start</i> and <i>end</i>.
     * 
     * @param start
     *            The first date on which set a presence.
     * @param end
     *            The last date on which set a presence.
     */
    public void addPresences(Date start, Date end)
    {
        DateWrapper beginDate = new DateWrapper(start);
        DateWrapper endDate = new DateWrapper(end);

        while (beginDate.before(endDate.addDays(1)))
        {
            int dayInWeek = beginDate.getDayInWeek();
            if (dayInWeek != 0 && dayInWeek != 6)
            {
                this.addPresence(beginDate);
            }
            else
            {
                this.addWeekend(beginDate);
            }
            beginDate = beginDate.addDays(1);
        }
    }

    /**
     * Create an absence key-value (date-P) for the date given in parameter.
     * 
     * @param date
     *            The date on which set an absence.
     */
    public void addAbsence(DateWrapper date)
    {
        String key = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
        Character event = this.get(key);
        if (event != null && event != 'E')
        {
            this.set(key, ABSENCE);
        }
    }

    /**
     * Create an absence key-value (date-A) for the all dates between <i>start</i> and <i>end</i>.
     * 
     * @param start
     *            The first date on which set an absence.
     * @param end
     *            The last date on which set an absence.
     */
    public void addAbsences(Date begin, Date end)
    {
        DateWrapper beginDate = new DateWrapper(begin);
        DateWrapper endDate = new DateWrapper(end);

        while (beginDate.before(endDate.addDays(1)))
        {
            int dayInWeek = beginDate.getDayInWeek();
            if (dayInWeek > 0 && dayInWeek < 6)
            {
                this.addAbsence(beginDate);
            }
            beginDate = beginDate.addDays(1);
        }
    }

    /**
     * Create an event key-value (date-P) for the date given in parameter.
     * 
     * @param date
     *            The date on which set an event.
     */
    public void addEvent(DateWrapper date)
    {
        String key = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
        this.set(key, EVENT);
    }

    /**
     * Create an event key-value (date-E) for the all dates between <i>start</i> and <i>end</i>.
     * 
     * @param start
     *            The first date on which set an event.
     * @param end
     *            The last date on which set an event.
     */
    public void addEvents(Date begin, Date end)
    {
        DateWrapper beginDate = new DateWrapper(begin);
        DateWrapper endDate = new DateWrapper(end);

        while (beginDate.before(endDate.addDays(1)))
        {
            this.addEvent(beginDate);
            beginDate = beginDate.addDays(1);
        }
    }

    /**
     * Update all day date from the template given in parameter.
     * 
     * @param template
     *            Row template.
     */
    public void updateFromTemplate(AbsenceRowModelData template)
    {
        this.map.clear();
        this.map.putAll(template.getProperties());
        this.setName(PersonNameUtils.getFullName(person.getLastName(), person.getFirstName(), person.getLogin()));
    }

    public void clearAbsences()
    {
        for (Map.Entry<String, Object> entry : this.map.entrySet())
        {
            if (entry.getValue() instanceof Character && entry.getValue().equals(ABSENCE))
            {
                map.put(entry.getKey(), PRESENCE);
            }
        }
    }
}
