package fr.hd3d.humanresources.ui.client.model.reader;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;


/**
 * Reader that read projects json data but returns record model data object (useful for generic widgets).
 * 
 * @author HD3D
 */
public class ProjectRecordReader extends Hd3dListJsonReader<RecordModelData>
{
    /**
     * Default constructor. Sets the Record Model Type into the reader.
     */
    public ProjectRecordReader()
    {
        super(ProjectModelData.getModelType());
    }

    @Override
    public RecordModelData newModelInstance()
    {
        return new RecordModelData();
    }
}
