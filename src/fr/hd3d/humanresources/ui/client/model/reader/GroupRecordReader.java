package fr.hd3d.humanresources.ui.client.model.reader;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


/**
 * Reader that read groups json data but returns record model data object (useful for generic widgets).
 * 
 * @author HD3D
 */
public class GroupRecordReader extends Hd3dListJsonReader<RecordModelData>
{
    /**
     * Default constructor. Sets the Record Model Type into the reader.
     */
    public GroupRecordReader()
    {
        super(ResourceGroupModelData.getModelType());
    }

    @Override
    public RecordModelData newModelInstance()
    {
        return new RecordModelData();
    }
}
