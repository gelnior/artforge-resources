package fr.hd3d.humanresources.ui.client.model;

import java.util.Date;
import java.util.HashMap;

import com.extjs.gxt.ui.client.data.BaseListLoader;
import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.listener.EventLoadListener;
import fr.hd3d.common.ui.client.modeldata.reader.AbsenceReader;
import fr.hd3d.common.ui.client.modeldata.reader.EventReader;
import fr.hd3d.common.ui.client.modeldata.reader.PersonReader;
import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.EventModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.service.proxy.ServicesProxy;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.model.modeldata.AbsenceRowModelData;


/**
 * Model handling absence data : person list, events and absences.
 * 
 * @author HD3D
 */
public class AbsenceModel
{
    /** Store containing persons to display in the absence grid. */
    private final ListStore<PersonModelData> persons;
    /** Store containing events to display in the absence grid. */
    private final ListStore<EventModelData> events;
    /** Store containing events to display in the absence grid. */
    private final ListStore<AbsenceModelData> absences;
    /** Absence row generated for the absence grid. */
    private final ListStore<AbsenceRowModelData> absenceRows = new ListStore<AbsenceRowModelData>();

    /** Reader used for person data parsing. */
    private final PersonReader reader = new PersonReader();
    /** Proxy used for retrieving person list. */
    private final ServicesProxy<PersonModelData> personProxy = new ServicesProxy<PersonModelData>(reader);
    /** Loader used for retrieving person list. */
    private final BaseListLoader<ListLoadResult<PersonModelData>> personLoader;

    /** Proxy used for retrieving event list. */
    private final ServicesProxy<EventModelData> eventProxy = new ServicesProxy<EventModelData>(new EventReader());
    /** Loader used for retrieving event list. */
    private final BaseListLoader<ListLoadResult<EventModelData>> eventLoader;

    /** Proxy used for retrieving absence list. */
    private final ServicesProxy<AbsenceModelData> absenceProxy = new ServicesProxy<AbsenceModelData>(
            new AbsenceReader());
    /** Loader used for retrieving absence list. */
    private final BaseListLoader<ListLoadResult<AbsenceModelData>> absenceLoader;

    /**
     * Default constructor, refresh person store and forwards ABSENCE_PERSONS_LOADED event when data are loaded.
     */
    public AbsenceModel()
    {
        personLoader = new BaseListLoader<ListLoadResult<PersonModelData>>(personProxy, reader);
        persons = new ListStore<PersonModelData>(personLoader);

        eventLoader = new BaseListLoader<ListLoadResult<EventModelData>>(eventProxy, reader);
        events = new ListStore<EventModelData>(eventLoader);

        absenceLoader = new BaseListLoader<ListLoadResult<AbsenceModelData>>(absenceProxy, reader);
        absences = new ListStore<AbsenceModelData>(absenceLoader);

        personLoader.addLoadListener(new EventLoadListener(HumanResourcesEvents.ABSENCE_PERSONS_LOADED));
        eventLoader.addLoadListener(new EventLoadListener(HumanResourcesEvents.ABSENCE_EVENTS_LOADED));
        absenceLoader.addLoadListener(new EventLoadListener(HumanResourcesEvents.ABSENCE_ABSENCES_LOADED));
    }

    /**
     * @return Store containing all absence rows.
     */
    public ListStore<AbsenceRowModelData> getAbsenceListStore()
    {
        return this.absenceRows;
    }

    /**
     * Set person proxy path.
     * 
     * @param path
     *            The path to set in the person proxy.
     */
    public void setPersonPath(String path)
    {
        this.personProxy.setPath(path);
    }

    /**
     * Load persons in the data store.
     */
    public void loadPersons()
    {
        this.personLoader.load();
    }

    /**
     * Load all events where its start date is between starDate and endDate or its end date is between startDate and
     * endDate.
     * 
     * @param startDate
     *            Lower limit for events start date and end date.
     * @param endDate
     *            Upper limit for events start date and end date.
     */
    public void loadEvents(Date startDate, Date endDate)
    {
        this.eventProxy.clearParameters();

        this.eventLoader.load();
    }

    /**
     * Load all absences where its start date is between starDate and endDate or its end date is between startDate and
     * endDate.
     * 
     * @param startDate
     *            Lower limit for events start date and end date.
     * @param endDate
     *            Upper limit for events start date and end date.
     */
    public void loadAbsences(Date startDate, Date endDate)
    {
        this.absenceProxy.clearParameters();
        this.absenceLoader.load();
    }

    /**
     * Clear absence row store.
     */
    public void clearAbsences()
    {
        this.absenceRows.removeAll();
    }

    /**
     * Refresh absences row store from startDate to endDate. First, it builds a template row where all weekend are set.
     * It put the event in the template. Then a row based on template is built for each person in person store and rows
     * are updated with absence data.
     * 
     * @param startDate
     *            First day of the created rows.
     * @param endDate
     *            End day of the created rows.
     */
    public void refreshAbsences(Date startDate, Date endDate)
    {
        HashMap<Long, AbsenceRowModelData> personRows = new HashMap<Long, AbsenceRowModelData>();

        this.absenceRows.removeAll();
        AbsenceRowModelData template = this.getRowTemplate(startDate, endDate);
        this.buildEmptyRows(template, personRows);
        this.updateAbsence(personRows);
    }

    /**
     * Build one row with no absence for each person in person store and fill a map to access to a row via the person
     * ID.
     * 
     * @param template
     *            Row template.
     * @param personRows
     *            A map associating a person id (key) to an absence row (value).
     */
    private void buildEmptyRows(AbsenceRowModelData template, HashMap<Long, AbsenceRowModelData> personRows)
    {
        for (PersonModelData person : persons.getModels())
        {
            AbsenceRowModelData row = new AbsenceRowModelData(person);

            row.updateFromTemplate(template);

            this.absenceRows.add(row);
            personRows.put(person.getId(), row);
        }
    }

    /**
     * Build a row template containing presence from startDate to endDate, week-ends and events.
     * 
     * @param startDate
     *            The first day of the row.
     * @param endDate
     *            The last day of the row.
     * @return The created template.
     */
    private AbsenceRowModelData getRowTemplate(Date startDate, Date endDate)
    {
        AbsenceRowModelData template = new AbsenceRowModelData(new PersonModelData());
        template.addPresences(startDate, endDate);
        for (EventModelData event : events.getModels())
        {
            template.addEvents(event.getStartDate(), event.getEndDate());
        }

        return template;
    }

    /**
     * For each absences in absence store, it updates the concerned row with new absence value
     * 
     * @param personRows
     *            A map associating a person id (key) to an absence row (value).
     */
    private void updateAbsence(HashMap<Long, AbsenceRowModelData> personRows)
    {
        for (AbsenceModelData absence : absences.getModels())
        {
            AbsenceRowModelData row = personRows.get(absence.getWorkerID());
            if (row != null)
            {
                row.addAbsences(absence.getStartDate(), absence.getEndDate());
            }
        }

        for (AbsenceRowModelData row : personRows.values())
        {
            this.absenceRows.update(row);
        }
    }

}
