package fr.hd3d.humanresources.ui.client.model;

import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.workobject.browser.model.WorkObjectModel;


/**
 * ContractModel contains contract view data.
 * 
 * @author HD3D
 */
public class ContractModel extends WorkObjectModel
{
   
    /** True when sheets are loaded. */
    private Boolean isSheetLoaded = false;

    /**
     * Default constructor, set up sheet combo box proxy.
     */
    public ContractModel()
    {

    }

    /**
     * @return True if sheets are loaded.
     */
    public Boolean getIsSheetLoaded()
    {
        return isSheetLoaded;
    }

    /**
     * Set isSheetLoaded.
     * 
     * @param isSheetLoaded
     *            The value for isSheetLoaded.
     */
    public void setIsSheetLoaded(Boolean isSheetLoaded)
    {
        this.isSheetLoaded = isSheetLoaded;
    }

}
