package fr.hd3d.humanresources.ui.client.model;

import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;


/**
 * This model does nothing. It is only present for maintenance and evolution purpose.
 * 
 * @author Hd3d
 */
public class HumanResourcesModel extends MainModel
{
    private static final String DATA_TYPE_FILE = "config/items_info_resource.json";

    
    /** Current entity is used to determine current active tabulation. */
    private String currentEntity = PersonModelData.SIMPLE_CLASS_NAME;

    public HumanResourcesModel()
    {
        SheetEditorModel.addDataFile(DATA_TYPE_FILE);
       // SheetEditorModel.setType(ESheetType.TEAM);
    }
    
    /**
     * @return Currently active tab.
     */
    public String getCurrentEntity()
    {
        return this.currentEntity;
    }

    /**
     * @param currentEntity
     *            Set currently active tab.
     */
    public void setCurrentEntity(String currentEntity)
    {
        this.currentEntity = currentEntity;
    }

}
