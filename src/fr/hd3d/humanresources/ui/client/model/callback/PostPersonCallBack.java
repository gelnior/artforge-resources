package fr.hd3d.humanresources.ui.client.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


public class PostPersonCallBack extends BaseCallback
{
    private final String name;

    public PostPersonCallBack(String name)
    {
        this.name = name;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
        Info.display("Save Result", "A new person is created : " + name);

        EventDispatcher.forwardEvent(ExplorerEvents.RELOAD_DATA);
    }
}
