package fr.hd3d.humanresources.ui.client.model.loader;

import java.util.List;

import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataLoader;


/**
 * ProjectTreeDataLoader retrieves data for project tree.
 * 
 * @author HD3D
 */
public class ProjectTreeDataLoader extends BaseTreeDataLoader<ResourceGroupModelData, ProjectModelData>
{

    /**
     * Default constructor.
     * 
     * @param proxy
     *            Proxy for retrieving data from server.
     * @param leafReader
     *            Reader used to parse leaf data.
     * @param parentReader
     *            Reader used to parse node data.
     */
    public ProjectTreeDataLoader(TreeServicesProxy<RecordModelData> proxy, IReader<ResourceGroupModelData> leafReader,
            IReader<ProjectModelData> parentReader)
    {
        super(proxy, leafReader, parentReader, new ResourceGroupModelData(), new ProjectModelData());
    }

    /**
     * If parent is null or is root, it loads all projects. If parent is a project, it loads groups linked to this
     * project.
     * 
     * @param parent
     *            The parent on which sons will be loaded.
     * 
     * @see fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataLoader#loadChildren(fr.hd3d.common.ui.client.modeldata.RecordModelData)
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean loadChildren(RecordModelData parent)
    {
        ProjectModelData parentInstance = getParentInstance();
        servicesProxy.clearParameters();
        servicesProxy.addParameter(new OrderBy(RecordModelData.NAME_FIELD));
        servicesProxy.setReader((IReader) parentReader);

        if (parent == null || parent.getId().longValue() == -1L)
        {
            String path = ServicesPath.getPath(ProjectModelData.SIMPLE_CLASS_NAME);
            servicesProxy
                    .addParameter(new EqConstraint(ProjectModelData.PROJECT_STATUS, EProjectStatus.OPEN.toString()));
            servicesProxy.setPath(path);

            this.children.add(parent);
            return load(parent);
        }
        else if (parentInstance.getClassName().equals(parent.getClassName()))
        {
            String path = parent.getDefaultPath() + "/"
                    + ServicesPath.getPath(ResourceGroupModelData.SIMPLE_CLASS_NAME);
            servicesProxy.setPath(path);

            this.children.add(parent);
            return load(parent);
        }
        else
        {
            return false;
        }
    }

    @Override
    protected void onNonRootLoadSuccess(RecordModelData parent, List<RecordModelData> result)
    {
        this.onLeavesLoadedSuccess(parent, result);
    }
}
