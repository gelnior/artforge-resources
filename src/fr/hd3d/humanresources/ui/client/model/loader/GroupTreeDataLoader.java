package fr.hd3d.humanresources.ui.client.model.loader;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataLoader;


/**
 * GroupTreeDataLoader retrieves data for group tree.
 * 
 * @author HD3D
 */
public class GroupTreeDataLoader extends BaseTreeDataLoader<PersonModelData, ResourceGroupModelData>
{

    /**
     * Default constructor.
     * 
     * @param proxy
     *            Proxy for retrieving data from server.
     * @param leafReader
     *            Reader used to parse leaf data.
     * @param parentReader
     *            Reader used to parse node data.
     */
    public GroupTreeDataLoader(TreeServicesProxy<RecordModelData> proxy, IReader<PersonModelData> leafReader,
            IReader<ResourceGroupModelData> parentReader)
    {
        super(proxy, leafReader, parentReader, new PersonModelData(), new ResourceGroupModelData());
    }

    /**
     * Load persons for group represented by <i>parent</i> (set appropriate order by).
     * 
     * @param parent
     *            The parent that need to get leaves loading.
     * 
     * @see fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataLoader#loadLeaves(fr.hd3d.common.ui.client.modeldata.RecordModelData)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void loadLeaves(RecordModelData parent)
    {
        this.servicesProxy.clearParameters();
        this.servicesProxy.setReader((IReader) leafReader);
        List<String> columns = new ArrayList<String>();
        columns.add(PersonModelData.PERSON_LAST_NAME_FIELD);
        columns.add(PersonModelData.PERSON_FIRST_NAME_FIELD);
        this.servicesProxy.addParameter(new OrderBy(columns));

        super.loadLeaves(parent);
    }

    /**
     * Load data that have no parents as root sons.
     * 
     * @param parent
     *            The parent root.
     * @param simpleClassName
     *            The class name of data retrieved.
     * 
     * @see fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataLoader#loadRoot(fr.hd3d.common.ui.client.modeldata.RecordModelData,
     *      java.lang.String)
     */
    @Override
    protected boolean loadRoot(RecordModelData parent, String simpleClassName)
    {
        String path = ServicesPath.getPath(ResourceGroupModelData.SIMPLE_CLASS_NAME) + ServicesPath.ROOTS;
        this.servicesProxy.setPath(path);

        this.children.add(parent);

        return load(parent);
    }
}
