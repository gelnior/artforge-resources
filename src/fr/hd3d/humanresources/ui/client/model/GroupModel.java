package fr.hd3d.humanresources.ui.client.model;

import com.extjs.gxt.ui.client.data.BaseListLoader;
import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.PersonReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.OrConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.proxy.ServicesProxy;
import fr.hd3d.humanresources.ui.client.model.loader.GroupTreeDataLoader;
import fr.hd3d.humanresources.ui.client.model.loader.ProjectTreeDataLoader;


/**
 * Model handling store for the group view : tree store of the group tree and list store of the person grid.
 * 
 * @author HD3D
 */
public class GroupModel
{
    /** Data loader for group tree. */
    private GroupTreeDataLoader loader;
    /** Store containing data for group tree. */
    private TreeStore<RecordModelData> groupStore;

    /** Data loader for project tree. */
    private ProjectTreeDataLoader projectLoader;
    /** Store containing data for project tree. */
    private TreeStore<RecordModelData> projectStore;

    /** Data proxy for person grid. */
    private final ServicesProxy<PersonModelData> personProxy = new ServicesProxy<PersonModelData>(new PersonReader());
    /** Data loader for person grid. */
    private final BaseListLoader<ListLoadResult<PersonModelData>> personLoader = new BaseListLoader<ListLoadResult<PersonModelData>>(
            personProxy);
    /** Store containing data for person grid. */
    private final ListStore<PersonModelData> personStore = new ListStore<PersonModelData>(personLoader);

    /** The proxy path constraint that will filter persons data by their last name. */
    private final Constraint loginFilter = new Constraint(EConstraintOperator.like, PersonModelData.PERSON_LOGIN_FIELD,
            "%", null);
    /** The proxy path constraint that will filter persons data by their last name. */
    private final Constraint firstNameFilter = new Constraint(EConstraintOperator.like,
            PersonModelData.PERSON_FIRST_NAME_FIELD, "%", null);
    /** The proxy path constraint that will filter persons data by their last name. */
    private final Constraint lastNameFilter = new Constraint(EConstraintOperator.like,
            PersonModelData.PERSON_LAST_NAME_FIELD, "%", null);

    private final OrConstraint filter;

    /**
     * Default constructor : sets parameters for person grid proxy.
     */
    public GroupModel()
    {
        OrderBy orderBy = new OrderBy(PersonModelData.PERSON_LAST_NAME_FIELD);
        this.personProxy.addParameter(orderBy);
        filter = new OrConstraint(loginFilter, new OrConstraint(firstNameFilter, lastNameFilter));
        this.personProxy.addParameter(filter);
    }

    /**
     * @return Group tree store.
     */
    public TreeStore<RecordModelData> getGroupStore()
    {
        return groupStore;
    }

    /**
     * Set group tree store
     * 
     * @param store
     *            The store to set.
     */
    public void setGroupStore(TreeStore<RecordModelData> store)
    {
        this.groupStore = store;
    }

    /**
     * Initialize group tree store data with server data.
     */
    public void initTree()
    {}

    /**
     * @return Store of the person grid.
     */
    public ListStore<PersonModelData> getPersonStore()
    {
        return this.personStore;
    }

    public void setConstraint(String rawValue)
    {
        rawValue = rawValue + "%";
        loginFilter.setLeftMember(rawValue);
        lastNameFilter.setLeftMember(rawValue);
        firstNameFilter.setLeftMember(rawValue);
    }

    /**
     * Reload person list.
     */
    public void reloadDataList()
    {
        this.personLoader.load();
    }

    /**
     * @return Group Tree loader.
     */
    public GroupTreeDataLoader getGroupTreeLoader()
    {
        return loader;
    }

    /**
     * Set group tree loader.
     * 
     * @param loader
     *            The loader to set.
     */
    public void setGroupLoader(GroupTreeDataLoader loader)
    {
        this.loader = loader;
    }

    /**
     * @return Project tree loader.
     */
    public ProjectTreeDataLoader getProjectLoader()
    {
        return projectLoader;
    }

    /**
     * Set project tree loader.
     * 
     * @param loader
     *            The loader to set.
     */
    public void setProjectLoader(ProjectTreeDataLoader projectLoader)
    {
        this.projectLoader = projectLoader;
    }

    /**
     * @return Project tree store.
     */
    public TreeStore<RecordModelData> getProjectStore()
    {
        return projectStore;
    }

    /**
     * Set group tree store.
     * 
     * @param loader
     *            The store to set.
     */
    public void setProjectStore(TreeStore<RecordModelData> store)
    {
        this.projectStore = store;
    }
}
