package fr.hd3d.humanresources.ui.client;

import com.google.gwt.core.client.EntryPoint;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.humanresources.ui.client.view.HumanResourcesView;


/**
 * Entry point class for Human Resource application..
 */
public class HumanResources implements EntryPoint
{
    /** This is the entry point method. It sets up the controllers and raise the initialization event. */
    public void onModuleLoad()
    {
        // PermissionUtil.setDebugOn();

        HumanResourcesView mainView = new HumanResourcesView();
        mainView.init();
        EventDispatcher.forwardEvent(CommonEvents.START);
    }
}
