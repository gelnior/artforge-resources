package fr.hd3d.humanresources.ui.client.controller;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.humanresources.ui.client.error.HumanResourcesErrors;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.model.AbsenceModel;
import fr.hd3d.humanresources.ui.client.model.modeldata.AbsenceRowModelData;
import fr.hd3d.humanresources.ui.client.view.IAbsenceView;


/**
 * Controller that handles absence events.
 * 
 * @author HD3D
 */
public class AbsenceController extends Controller
{
    /** Model handling absence data. */
    private final AbsenceModel model;
    /** View displaying absence and events. */
    private final IAbsenceView view;

    /**
     * Default constructor.
     * 
     * @param model
     *            Model handling absence data.
     * @param view
     *            View displaying absence and events.
     */
    public AbsenceController(AbsenceModel model, IAbsenceView view)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == HumanResourcesEvents.ABSENCE_GRID_CREATED)
        {
            this.onAbsenceGridCreated();
        }
        else if (type == HumanResourcesEvents.ABSENCE_PERSONS_LOADED)
        {
            this.onPersonsLoaded();
        }
        else if (type == HumanResourcesEvents.ABSENCE_EVENTS_LOADED)
        {
            this.onEventsLoaded();
        }
        else if (type == HumanResourcesEvents.ABSENCE_ABSENCES_LOADED)
        {
            this.onAbsencesLoaded();
        }
        else if (type == HumanResourcesEvents.ABSENCE_FILTER_CHANGED)
        {
            this.onFilterChanged();
        }
        else if (type == HumanResourcesEvents.ABSENCE_GRID_CHANGED)
        {
            this.onAbsenceGridChanged();
        }
        else if (type == HumanResourcesEvents.ABSENCE_EVENT_EDITOR_CLICKED)
        {
            this.onEventEditorClicked();
        }
        else if (type == HumanResourcesEvents.ABSENCE_ABSENCE_EDITOR_CLICKED)
        {
            this.onAbsenceEditorClicked();
        }
        else if (type == HumanResourcesEvents.ABSENCE_GROUP_CHANGED)
        {
            this.onAbsenceGroupChanged();
        }
        else if (type == HumanResourcesEvents.ABSENCE_REFRESH_CLICKED)
        {
            this.onRefreshClicked();
        }
        else if (type == HumanResourcesEvents.ABSENCE_ABSENCE_EDITOR_HIDE)
        {
            this.onAbsenceEditorHide(event);
        }
        else if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            this.onPermissionsInitialized();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When date filters changed, absence grid is rebuilt to show absences/events of the selected period. The grid is
     * hidden until all data are set.
     */
    private void onFilterChanged()
    {
        this.model.clearAbsences();

        this.view.maskGrid();
        this.view.rebuildGrid();
    }

    /**
     * When absence grid is created, it loads person data.
     */
    private void onAbsenceGridCreated()
    {
        this.model.loadPersons();
    }

    /**
     * When absence grid changed, absence rows are built for the period selected by the user via the date filters.
     */
    private void onAbsenceGridChanged()
    {
        Date startDate = this.view.getStartDate();
        Date endDate = this.view.getEndDate();

        this.model.loadEvents(startDate, endDate);
    }

    /**
     * When persons are loaded, it loads event data.
     */
    private void onPersonsLoaded()
    {
        Date startDate = this.view.getStartDate();
        Date endDate = this.view.getEndDate();

        this.model.loadEvents(startDate, endDate);
    }

    /**
     * When events are loaded, it loads absence data.
     */
    private void onEventsLoaded()
    {
        Date startDate = this.view.getStartDate();
        Date endDate = this.view.getEndDate();

        this.model.loadAbsences(startDate, endDate);
    }

    /**
     * When absences are loaded, absence grid is updated with all data (persons, events and absences) then the grid is
     * unmasked.
     */
    private void onAbsencesLoaded()
    {
        Date startDate = this.view.getStartDate();
        Date endDate = this.view.getEndDate();

        this.model.refreshAbsences(startDate, endDate);
        this.view.unMaskGrid();

        if (this.model.getAbsenceListStore().getCount() > 0)
        {
            this.view.setAbsenceButtonEnabled(true);
            // this.view.setDatePickerEnabled(true);
            // this.view.setRefreshEnabled(true);
        }
        else
        {
            this.view.setAbsenceButtonEnabled(false);
            // this.view.setDatePickerEnabled(false);
            // this.view.setRefreshEnabled(false);
        }
    }

    /**
     * It clears then reload absence rows.
     */
    private void onRefreshClicked()
    {
        this.model.clearAbsences();

        this.onAbsenceGridCreated();
    }

    /**
     * When group selection changes, it updates the absence store data and the person proxy path.
     */
    private void onAbsenceGroupChanged()
    {
        String path = ServicesPath.getPath(ResourceGroupModelData.SIMPLE_CLASS_NAME);
        path += this.view.getCurrentGroup().getId() + "/";
        path += ServicesPath.getPath(PersonModelData.SIMPLE_CLASS_NAME);

        this.model.setPersonPath(path);
        this.model.loadPersons();
    }

    /**
     * Show event editor.
     */
    private void onEventEditorClicked()
    {
        this.view.showEventEditor();
    }

    /**
     * Show absence editor only if there is one row selected. If there is no selection or if more than one row is
     * selected, it forwards an error.
     */
    private void onAbsenceEditorClicked()
    {
        List<AbsenceRowModelData> selection = this.view.getSelection();

        if (selection.size() == 1)
        {
            this.view.showAbsenceEditor();
        }
        else if (selection.size() == 0)
        {
            ErrorDispatcher.sendError(HumanResourcesErrors.NO_SELECTION);
        }
        else
        {
            ErrorDispatcher.sendError(HumanResourcesErrors.TOO_BIG_SELECTION);
        }
    }

    /**
     * When absence editor is hided, the modified row is updated.
     * 
     * @param event
     *            Absence editor hidden.
     */
    private void onAbsenceEditorHide(AppEvent event)
    {
        AbsenceRowModelData row = this.view.getAbsenceEditorSelection();
        row.clearAbsences();

        for (AbsenceModelData absence : this.view.getAbsencesFromEditor())
        {
            if (absence.getStartDate() != null && absence.getEndDate() != null)
            {
                row.addAbsences(absence.getStartDate(), absence.getEndDate());
            }
        }

        this.model.getAbsenceListStore().update(row);
    }

    /**
     * When permissions are initialized, widgets are updated.
     */
    private void onPermissionsInitialized()
    {
        this.view.initEditorPermissions();
    }

    /**
     * Register events supported by controller.
     */
    private void registerEvents()
    {
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_FILTER_CHANGED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_GRID_CREATED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_GRID_CHANGED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_PERSONS_LOADED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_EVENTS_LOADED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_ABSENCES_LOADED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_CREATE_ABSENCE_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_CREATE_EVENT_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_ABSENCE_SAVE_SUCCESS);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_EVENT_SAVE_SUCCESS);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_EVENT_EDITOR_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_ABSENCE_EDITOR_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_GROUP_CHANGED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_REFRESH_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.ABSENCE_ABSENCE_EDITOR_HIDE);

        this.registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
    }

}
