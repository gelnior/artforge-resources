package fr.hd3d.humanresources.ui.client.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.identitysheet.events.IdentitySheetEvents;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationEvents;
import fr.hd3d.common.ui.client.widget.workobject.browser.controller.WorkObjectController;
import fr.hd3d.humanresources.ui.client.config.HumanResourceConfig;
import fr.hd3d.humanresources.ui.client.error.HumanResourcesErrors;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.model.PersonModel;
import fr.hd3d.humanresources.ui.client.view.PersonView;


/**
 * PersonController handles Person editor view events.
 * 
 * @author HD3D
 */
public class PersonController extends WorkObjectController
{
    /** Model for persons data. */
    private final PersonModel model;
    /** View that display person editor. */
    private final PersonView view;

    private int nbDeletion = 0;

    /**
     * Default constructor.
     * 
     * @param view
     *            View that display person editor.
     * @param model
     *            Model for persons data.
     */
    public PersonController(PersonView view, PersonModel model)
    {
        super(view, model);

        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        EventType type = event.getType();

        if (type == HumanResourcesEvents.LOAD_EXPLORER_DATA)
        {
            this.loadExplorerData(event);
        }
        else if (type == ExplorerEvents.SHEET_INFORMATIONS_LOADED)
        {
            this.model.setIsSheetLoaded(true);
        }
        else if (type == ExplorerEvents.BEFORE_SAVE)
        {
            this.view.showSaving();
        }
        else if (type == IdentitySheetEvents.DATAS_SAVED)
        {
            this.dataSaved();
        }
        else if (type == HumanResourcesEvents.PERSON_CREATED)
        {
            this.dataSaved();
        }
        else if (type == ExplorerEvents.AFTER_SAVE)
        {
            this.view.hideSaving();
        }
        else if (type == ExplorerEvents.SELECT_CHANGE)
        {
            this.onSelectChange(event);
        }
        else if (type == HumanResourcesEvents.PERSON_SHEET_CHANGED)
        {
            this.onSheetChanged();
        }
        else if (type == RelationEvents.DIALOG_CLOSED)
        {
            this.view.refreshIdentityPanel(null);
        }
        else if (type == HumanResourcesEvents.SHOW_NEW_PERSON_WINDOW)
        {
            this.view.displayForm(event);
        }
        else if (type == HumanResourcesEvents.CREATE_A_PERSON)
        {
            createAPerson(event);
        }
        else if (type == HumanResourcesEvents.RELATION_PERSON_GROUP_CLICKED)
        {
            this.onRelationPersonGroupClicked();
        }
        else if (type == HumanResourcesEvents.DELETE_PERSON_CLICKED)
        {
            this.onDeletePerson();
        }
        else if (type == HumanResourcesEvents.DELETE_PERSON_CONFIRMED)
        {
            this.onDeletePersonConfirmed();
        }
        else if (type == HumanResourcesEvents.PERSON_DELETED)
        {
            this.onPersonDeleted();
        }
        else if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            this.onPermissionsInitialized();
        }
        else if (type == CommonEvents.ERROR)
        {
            this.view.hideSaving();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When server sends deletion success, it checks if all deletion are done. If all deletion are done, the explorer
     * data are reloaded.
     */
    private void onPersonDeleted()
    {
        nbDeletion--;
        if (nbDeletion == 0)
        {
            EventDispatcher.forwardEvent(ExplorerEvents.RELOAD_DATA);
            this.view.enablePersonDeleteButton(true);
        }
    }

    /**
     * When Delete Person button is clicked, it checks if selection contains at least one person. If it's ok a
     * confirmation dialog box is displayed either an error message is displayed..
     */
    private void onDeletePerson()
    {
        if (this.view.getSelectedModels() != null && this.view.getSelectedModels().size() > 0)
        {
            this.view.showDeletePersonConfirmation();
        }
        else
        {
            ErrorDispatcher.sendError(HumanResourcesErrors.NO_SELECTION);
        }
    }

    /**
     * When delete person is confirmed, selected rows are deleted from services. Explorer is masked during deletion
     * operations.
     */
    private void onDeletePersonConfirmed()
    {
        List<Hd3dModelData> selectedPersons = this.view.getSelectedModels();
        nbDeletion = selectedPersons.size();
        this.view.enablePersonDeleteButton(false);
        this.view.maskExplorer();

        for (Hd3dModelData selectedPerson : selectedPersons)
        {
            selectedPerson.delete(HumanResourcesEvents.PERSON_DELETED);
        }
    }

    /**
     * When view saving is finished, the view is added to the view combo box and the view is loaded in the explorer.
     * 
     * @param event
     *            The end save event.
     */
    // private void onEndSave(AppEvent event)
    // {
    // SheetModelData view = (SheetModelData) event.getData();
    // ListStore<SheetModelData> store = this.model.getViewStore();
    // if (store.indexOf(view) == -1)
    // {
    // store.add(view);
    // }
    // this.view.setExplorerView(view);
    //
    // AppEvent event2 = new AppEvent(HumanResourcesEvents.INIT_SHEET, view);
    // EventDispatcher.forwardEvent(event2);
    // }

    /**
     * When relation person-group button is clicked, the relation tool is shown. If there is no selection before tool
     * displaying, an error message is displayed.
     */
    private void onRelationPersonGroupClicked()
    {
        if (this.view.getSelectedModels().size() > 0)
        {
            this.view.showRelationTool(this.view.getSelectedModels());
        }
        else
        {
            ErrorDispatcher.sendError(HumanResourcesErrors.NO_SELECTION);
        }
    }

    /**
     * When view changed, identity panel is cleared and last selected sheet id is saved to the favorite cookie.
     */
    private void onSheetChanged()
    {
        this.view.clearIdentityPanel();
        SheetModelData sheet = this.view.getSelectedSheet();
        if (sheet != null)
        {
            this.view.putFavCookieValue(HumanResourceConfig.COOKIE_SHEET_VALUE, sheet.getId().toString());
        }
        this.view.refreshExplorerData(sheet);
    }

    /**
     * When selection changes, the identity panel is updated.
     * 
     * @param event
     *            Selection change event.
     */
    private void onSelectChange(AppEvent event)
    {
        Hd3dModelData record = event.getData(ExplorerEvents.EVENT_VAR_MODELDATA);

        if (record != null && record.getId() != null && this.view.getSelectedModels().size() == 1)
        {
            this.view.refreshIdentityPanel(record.getId());
        }
        else
        {
            this.view.clearIdentityPanel();
        }
    }

    /**
     * Reload data displayed by the explorer.
     * 
     * @param event
     *            Load Explorer Data event.
     */
    private void loadExplorerData(AppEvent event)
    {
        EventDispatcher.forwardEvent(ExplorerEvents.RELOAD_DATA);
    }

    /**
     * When an external widget changes data, the explorer data are reloaded.
     */
    private void dataSaved()
    {
        EventDispatcher.forwardEvent(ExplorerEvents.RELOAD_DATA);
    }

    /**
     * When permissions are initialized, tool bar buttons are hidden or shown depending on user permissions.
     */
    public void onPermissionsInitialized()
    {
        this.view.setCreatePersonToolITemVisible(PermissionUtil.hasCreateRights(PersonModelData.SIMPLE_CLASS_NAME));
        this.view.setRelationToolITemVisible(PermissionUtil.hasUpdateRights(PersonModelData.SIMPLE_CLASS_NAME));
        this.view.setSaveToolITemVisible(PermissionUtil.hasUpdateRights(PersonModelData.SIMPLE_CLASS_NAME));
    }

    /**
     * When person dialog "OK" button is clicked, the person data set by the user are saved to the database.
     * 
     * @param event
     *            Person dialog "OK" button clicked event.
     */
    public void createAPerson(AppEvent event)
    {
        String firstName = event.getData("firstName");
        String lastName = event.getData("lastName");
        String login = event.getData("login");

        PersonModelData person = new PersonModelData();
        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setLogin(login);

        person.save(HumanResourcesEvents.PERSON_CREATED);
    }

    /** Register all events the controller can handle. */
    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(HumanResourcesEvents.INIT_VIEW);
        this.registerEventTypes(HumanResourcesEvents.LOAD_EXPLORER_DATA);
        this.registerEventTypes(HumanResourcesEvents.INIT_SHEET);
        this.registerEventTypes(ExplorerEvents.SHEET_INFORMATIONS_LOADED);
        this.registerEventTypes(ExplorerEvents.BEFORE_SAVE);
        this.registerEventTypes(ExplorerEvents.AFTER_SAVE);
        this.registerEventTypes(ExplorerEvents.SELECT_CHANGE);
        this.registerEventTypes(HumanResourcesEvents.PERSON_SHEET_CHANGED);
        this.registerEventTypes(HumanResourcesEvents.SHEET_DELETED);
        this.registerEventTypes(RelationEvents.DIALOG_CLOSED);
        this.registerEventTypes(IdentitySheetEvents.DATAS_SAVED);
        this.registerEventTypes(IdentitySheetEvents.SEND_RENDERER);
        this.registerEventTypes(HumanResourcesEvents.SHOW_NEW_PERSON_WINDOW);
        this.registerEventTypes(HumanResourcesEvents.CREATE_A_PERSON);
        this.registerEventTypes(HumanResourcesEvents.RELATION_PERSON_GROUP_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.LOAD_GROUPS);
        this.registerEventTypes(HumanResourcesEvents.PERSON_CREATED);
        this.registerEventTypes(HumanResourcesEvents.DELETE_PERSON_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.DELETE_PERSON_CONFIRMED);
        this.registerEventTypes(HumanResourcesEvents.PERSON_CREATED);
        this.registerEventTypes(HumanResourcesEvents.PERSON_DELETED);
        this.registerEventTypes(CommonEvents.ERROR);
        this.registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
    }

}
