package fr.hd3d.humanresources.ui.client.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ContractModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainController;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.model.HumanResourcesModel;
import fr.hd3d.humanresources.ui.client.view.ContractView;
import fr.hd3d.humanresources.ui.client.view.HumanResourcesView;
import fr.hd3d.humanresources.ui.client.view.PersonView;


/**
 * HumanResourcesController manages the main events.
 * 
 * @author HD3D
 */
public class HumanResourcesController extends MainController
{
    /** Model handling main data like services path. */
    private final HumanResourcesModel model;
    /** View that contains all the application views. */
    private final HumanResourcesView view;

    /**
     * Default constructor.
     * 
     * @param view
     *            View that contains all the application views.
     * @param model
     *            Model handling main data like services path.
     */
    public HumanResourcesController(HumanResourcesView view, HumanResourcesModel model)
    {
        super(model, view);

        this.model = model;
        this.view = view;
    }

    /**
     * @return Model handling main data like services path.
     */
    public HumanResourcesModel getModel()
    {
        return model;
    }

    /**
     * @return View that contains all the application views.
     */
    public HumanResourcesView getView()
    {
        return view;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        EventType type = event.getType();
        {
            if (type == HumanResourcesEvents.INIT_VIEW)
            {}
            else if (type == HumanResourcesEvents.PERSON_TAB_CLICKED)
            {
                this.onPersonTabClicked();
            }
            else if (type == HumanResourcesEvents.CONTRACT_TAB_CLICKED)
            {
                this.onContractTabClicked();
            }
            else if (type == ExplorerEvents.SELECT_CHANGE)
            {
                this.onExplorerSelectChange(event);
                this.forwardToChild(event);
            }
            else
            {
                this.forwardToChild(event);
            }
        }
    }

    /**
     * When settings are initialized, excluded fields are set then INIT_VIEW event is forwarded.
     */
    @Override
    protected void onSettingInitialized(AppEvent event)
    {
        ExcludedField.addExcludedField(Hd3dModelData.TAG_IDS_FIELD);
        ExcludedField.addExcludedField(Hd3dModelData.TAG_NAMES_FIELD);
        ExcludedField.addExcludedField(PersonModelData.TASKS_IDS_FIELD);
        ExcludedField.addExcludedField(PersonModelData.TASKS_NAMES_FIELD);

        ExcludedField.addExcludedField("days");
        ExcludedField.addExcludedField("contracts");
        ExcludedField.addExcludedField("ledResourceGroups");

        ExcludedField.addExcludedField("activitiesDuration");
        ExcludedField.addExcludedField("approvalNotes");
        ExcludedField.addExcludedField("boundEntityTaskLink");
        ExcludedField.addExcludedField("boundTasks");
        ExcludedField.addExcludedField("nbTaskOk");
        ExcludedField.addExcludedField("taskActivities");
        ExcludedField.addExcludedField("taskChanges");
        ExcludedField.addExcludedField("taskGroup");
        ExcludedField.addExcludedField("tasksActivitiesDuration");
        ExcludedField.addExcludedField("tasksDurationGap");
        ExcludedField.addExcludedField("tasksEstimation");
        ExcludedField.addExcludedField("tasksStatus");
        ExcludedField.addExcludedField("tasksWorkers");
        ExcludedField.addExcludedField("version");
        ExcludedField.addExcludedField("skillLevels");

        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TaskDurationCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskCANCELLEDCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskCLOSEDCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskOKCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskSTANDBYCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskWAITAPPCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskWIPCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TotalNbOfTasksCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TaskWorkersCollectionQuery");

        this.initView();
    }

    /**
     * Initialize view widgets and hist loading panel.
     */
    private void initView()
    {
        this.view.initWidgets();
        // this.addChild(this.view.getContractPanel().getController());
        this.addChild(this.view.getPersonPanel().getController());
        this.view.hideStartPanel();
        EventDispatcher.forwardEvent(HumanResourcesEvents.PERSON_TAB_CLICKED);
    }

    private void onPersonTabClicked()
    {
        this.view.saveTabToCookie(PersonView.NAME);
        this.view.idleContractView();
        this.view.unidlePersonView();
        this.model.setCurrentEntity(PersonModelData.SIMPLE_CLASS_NAME);
        this.view.initPersonSheets();

    }

    private void onContractTabClicked()
    {
        this.view.saveTabToCookie(ContractView.NAME);
        this.view.idlePersonView();
        this.view.unidleContractView();
        this.model.setCurrentEntity(ContractModelData.SIMPLE_CLASS_NAME);
        this.view.initContractSheets();

    }

    /**
     * When explorer selection changes, tag editor is updated with currently selected record tags.
     * 
     * @param event
     *            Explorer selection change event.
     */
    private void onExplorerSelectChange(AppEvent event)
    {
        Hd3dModelData selectedModel = event.getData(ExplorerEvents.EVENT_VAR_MODELDATA);
        if (selectedModel != null)
        {
            if (PersonModelData.SIMPLE_CLASS_NAME.equals(this.model.getCurrentEntity()))
            {
                selectedModel.setSimpleClassName(PersonModelData.SIMPLE_CLASS_NAME);
                selectedModel.setClassName(PersonModelData.CLASS_NAME);
            }
            else
            {
                selectedModel.setSimpleClassName(ContractModelData.SIMPLE_CLASS_NAME);
                selectedModel.setClassName(ContractModelData.CLASS_NAME);
            }
        }
    }

    /** Register all events the controller can handle. */
    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(HumanResourcesEvents.PERSON_TAB_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.CONTRACT_TAB_CLICKED);
        this.registerEventTypes(ExplorerEvents.SELECT_CHANGE);

        this.registerEventTypes(CommonEvents.ERROR);
    }
}
