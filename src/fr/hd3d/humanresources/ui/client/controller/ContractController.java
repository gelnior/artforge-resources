package fr.hd3d.humanresources.ui.client.controller;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ContractModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.identitysheet.events.IdentitySheetEvents;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.workobject.browser.controller.WorkObjectController;
import fr.hd3d.humanresources.ui.client.error.HumanResourcesErrors;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.model.ContractModel;
import fr.hd3d.humanresources.ui.client.view.ContractView;


/**
 * PersonController handles Person editor view events.
 * 
 * @author HD3D
 */
public class ContractController extends WorkObjectController
{
    /** Model for persons data. */
    private final ContractModel model;
    /** View that display person editor. */
    private final ContractView view;

    int nbDeletion = 0;

    /**
     * Default constructor.
     * 
     * @param view
     *            View that display person editor.
     * @param model
     *            Model for persons data.
     */
    public ContractController(ContractView view, ContractModel model)
    {
        super(view, model);

        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        EventType type = event.getType();

        if (type == HumanResourcesEvents.CONTRACT_CREATED)
        {
            this.dataSaved();
        }
        else if (type == HumanResourcesEvents.NEW_CONTRACT_CLICKED)
        {
            this.view.displayForm(event);
        }
        else if (type == HumanResourcesEvents.NEW_CONTRACT_AMENDMENT_CLICKED)
        {
            this.view.displayFormAmendment(event);
        }
        else if (type == HumanResourcesEvents.CREATE_CONTRACT_REQUESTED)
        {
            this.createAContract(event);
        }
        else if (type == HumanResourcesEvents.DELETE_CONTRACT_CLICKED)
        {
            this.onDeleteContract();
        }
        else if (type == HumanResourcesEvents.DELETE_CONTRACT_CONFIRMED)
        {
            this.onDeleteContractConfirmed();
        }
        else if (type == HumanResourcesEvents.CONTRACT_DELETED)
        {
            this.onContractDeleted();
        }
        else if (type == HumanResourcesEvents.CONTRACT_PERSON_CHANGED)
        {
            this.onContractPersonChange();
        }
        else if (type == CommonEvents.ERROR)
        {
            this.view.hideSaving();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When server sends deletion success, it checks if all deletion are done. If all deletion are done, the explorer
     * data are reloaded.
     */
    private void onContractDeleted()
    {
        nbDeletion--;
        if (nbDeletion == 0)
        {
            EventDispatcher.forwardEvent(ExplorerEvents.RELOAD_DATA);
        }
    }

    /**
     * When Delete Contract button is clicked, it checks if selection contains at least one Contract. If it's ok a
     * confirmation dialog box is displayed either an error message is displayed..
     */
    private void onDeleteContract()
    {
        if (this.view.getSelection() != null && this.view.getSelection().size() > 0)
        {
            this.view.showDeleteContractConfirmation();
        }
        else
        {
            ErrorDispatcher.sendError(HumanResourcesErrors.NO_SELECTION);
        }
    }

    /**
     * When delete Contract is confirmed, selected rows are deleted from services. Explorer is masked during deletion
     * operations.
     */
    private void onDeleteContractConfirmed()
    {
        List<Hd3dModelData> selectedContracts = this.view.getSelection();
        nbDeletion = selectedContracts.size();
        this.view.unmask();

        for (Hd3dModelData selectedContract : selectedContracts)
        {
            selectedContract.delete(HumanResourcesEvents.CONTRACT_DELETED);
        }
    }

    public void onContractPersonChange()
    {
        this.view.contractDialog.updateOkButtonStatus();
    }

    /**
     * When an external widget changes data, the explorer data are reloaded.
     */
    private void dataSaved()
    {
        EventDispatcher.forwardEvent(ExplorerEvents.RELOAD_DATA);
    }

    /**
     * When Contract dialog "OK" button is clicked, the Contract data set by the user are saved to the database.
     * 
     * @param event
     *            Contract dialog "OK" button clicked event.
     */
    public void createAContract(AppEvent event)
    {
        PersonModelData person = event.getData("person");
        String job = event.getData("job");
        Float salary = event.getData("salary");
        String type = event.getData("type");
        Date startDate = event.getData("startDate");
        Date endDate = event.getData("endDate");

        ContractModelData contract = new ContractModelData();
        contract.setPersonId(person.getId());
        contract.setName(job);
        contract.setType(type);
        contract.setStartDate(startDate);
        contract.setEndDate(endDate);
        contract.setSalary(salary);

        contract.save(HumanResourcesEvents.CONTRACT_CREATED);
    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(HumanResourcesEvents.INIT_VIEW);
        this.registerEventTypes(HumanResourcesEvents.LOAD_EXPLORER_DATA);
        this.registerEventTypes(HumanResourcesEvents.INIT_SHEET);
        this.registerEventTypes(ExplorerEvents.SHEET_INFORMATIONS_LOADED);
        this.registerEventTypes(ExplorerEvents.BEFORE_SAVE);
        this.registerEventTypes(ExplorerEvents.AFTER_SAVE);
        this.registerEventTypes(ExplorerEvents.SELECT_CHANGE);
        this.registerEventTypes(HumanResourcesEvents.CONTRACT_SHEET_CHANGED);
        this.registerEventTypes(HumanResourcesEvents.SHEET_DELETED);
        this.registerEventTypes(RelationEvents.DIALOG_CLOSED);
        this.registerEventTypes(IdentitySheetEvents.DATAS_SAVED);
        this.registerEventTypes(IdentitySheetEvents.SEND_RENDERER);
        this.registerEventTypes(HumanResourcesEvents.NEW_CONTRACT_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.NEW_CONTRACT_AMENDMENT_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.CREATE_CONTRACT_REQUESTED);
        this.registerEventTypes(HumanResourcesEvents.RELATION_CONTRACT_GROUP_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.LOAD_GROUPS);
        this.registerEventTypes(HumanResourcesEvents.CONTRACT_CREATED);
        this.registerEventTypes(HumanResourcesEvents.DELETE_CONTRACT_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.DELETE_CONTRACT_CONFIRMED);
        this.registerEventTypes(HumanResourcesEvents.CONTRACT_CREATED);
        this.registerEventTypes(HumanResourcesEvents.CONTRACT_DELETED);
        this.registerEventTypes(CommonEvents.ERROR);
        this.registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
        this.registerEventTypes(HumanResourcesEvents.CONTRACT_PERSON_CHANGED);

    }

}
