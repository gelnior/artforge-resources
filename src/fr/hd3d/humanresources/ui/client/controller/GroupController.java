package fr.hd3d.humanresources.ui.client.controller;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.model.GroupModel;
import fr.hd3d.humanresources.ui.client.view.IGroupView;


/**
 * Controller handling group panel events. Group panel is used for handling relations between group, persons and
 * projects.
 * 
 * @author HD3D
 */
public class GroupController extends Controller
{
    /** Model which contains group panel data. */
    private final GroupModel model;
    /** View which displays group panel data. */
    private final IGroupView view;

    /**
     * Default constructor.
     * 
     * @param model
     *            Model which contains group panel data.
     * @param view
     *            View which displays group panel data.
     */
    public GroupController(GroupModel model, IGroupView view)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == HumanResourcesEvents.GROUP_GROUP_DROPPED)
        {
            this.onGroupDropped(event);
        }
        else if (type == HumanResourcesEvents.GROUP_PERSON_FILTER_CHANGED)
        {
            this.onPersonFilterChanged();
        }
        else if (type == HumanResourcesEvents.GROUP_PERSON_DROPPED)
        {
            this.onPersonDropped(event);
        }
        else if (type == HumanResourcesEvents.GROUP_GROUP_REFRESH_CLICKED)
        {
            this.onGroupRefreshClicked(event);
        }
        else if (type == HumanResourcesEvents.GROUP_FIRST_LOAD)
        {
            this.onFirstLoad(event);
        }
        else if (type == HumanResourcesEvents.GROUP_CREATE_GROUP_CLICKED)
        {
            this.onCreateGroupClicked();
        }
        else if (type == HumanResourcesEvents.GROUP_CREATE_GROUP_CONFIRMED)
        {
            this.onCreateGroupConfirmed(event);
        }
        else if (type == HumanResourcesEvents.GROUP_RENAME_GROUP_CLICKED)
        {
            this.onRenameGroupClicked();
        }
        else if (type == HumanResourcesEvents.GROUP_RENAME_GROUP_CONFIRMED)
        {
            this.onRenameGroupConfirmed(event);
        }
        else if (type == HumanResourcesEvents.GROUP_DELETE_GROUP_CLICKED)
        {
            this.onDeleteGroupClicked(event);
        }
        else if (type == HumanResourcesEvents.GROUP_MAKE_LEADER_CLICKED)
        {
            this.onMakeLeaderClicked(event);
        }
        else if (type == HumanResourcesEvents.GROUP_REMOVE_PERSON_CLICKED)
        {
            this.onRemovePersonClicked(event);
        }
        else if (type == HumanResourcesEvents.GROUP_PROJECT_REFRESH_CLICKED)
        {
            this.onProjectRefreshClicked(event);
        }
        else if (type == HumanResourcesEvents.GROUP_REMOVE_FROM_PROJECT_CLICKED)
        {
            this.onRemoveFromProjectClicked(event);
        }
        else if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            this.onPermissionsInitialized();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When person filter changed (used to find people in data base) the person store is reloaded with a new like
     * condition that depends on the field content.
     */
    private void onPersonFilterChanged()
    {
        this.model.setConstraint(this.view.getFilterValue());
        this.model.reloadDataList();
    }

    /**
     * When a group has to be removed from a project, project list associated to the group is updated (parent project is
     * removed), then the group is saved and removed from the project tree.
     * 
     * @param event
     *            Remove from project event.
     */
    private void onRemoveFromProjectClicked(AppEvent event)
    {
        List<RecordModelData> selection = this.view.getProjectSelection();
        if (selection.isEmpty())
            return;

        for (RecordModelData group : selection)
        {
            RecordModelData project = model.getProjectStore().getParent(group);
            List<Long> projectIds = project.get(ProjectModelData.RESOURCEGROUP_IDS_FIELD);
            if (projectIds == null)
                continue;

            projectIds.remove(group.getId());
            project.save();

            model.getProjectStore().remove(group);

        }
    }

    /**
     * When refresh project is clicked, the project leaves (groups) are reloaded then the corresponding node is
     * expanded.
     * 
     * @param event
     *            Refresh Project event.
     */
    private void onProjectRefreshClicked(AppEvent event)
    {
        List<RecordModelData> selection = this.view.getProjectSelection();
        if (selection.isEmpty())
            return;

        this.model.getProjectLoader().loadChildren(selection.get(0));
        this.model.getProjectStore().removeAll(selection.get(0));
        this.view.expandProject(selection.get(0));
    }

    /**
     * When Remove person is clicked, group list associated to the person is updated (parent project is removed), then
     * the person is saved and removed from the group tree.
     * 
     * @param event
     *            Remove person event.
     */
    private void onRemovePersonClicked(AppEvent event)
    {
        List<RecordModelData> selection = this.view.getGroupSelection();
        RecordModelData person = selection.get(0);
        RecordModelData group = model.getGroupStore().getParent(person);

        List<Long> groupIds = person.get(PersonModelData.RESOURCEGROUP_IDS_FIELD);
        groupIds.remove(group.getId());
        person.save();

        model.getGroupStore().remove(person);
    }

    /**
     * When make leader is clicked, the leader field of the parent group of the selected person is updated with selected
     * person id. Then the group node is refeshed.
     * 
     * @param event
     *            Make leader event.
     */
    private void onMakeLeaderClicked(AppEvent event)
    {
        List<RecordModelData> selection = this.view.getGroupSelection();
        RecordModelData person = selection.get(0);
        RecordModelData group = model.getGroupStore().getParent(person);
        group.set(ResourceGroupModelData.LEADER_ID_FIELD, person.getId());
        group.save();
        model.getGroupStore().update(group);
        this.view.refreshGroup(group);
    }

    /**
     * When delete group is clicked, the selected groups are deleted from database. Record are removed from tree, but
     * not all the group instance (too complicated). The whole tree should be updated after a deletion.
     * 
     * @param event
     *            Delete group event.
     */
    private void onDeleteGroupClicked(AppEvent event)
    {
        for (RecordModelData record : this.view.getGroupSelection())
        {
            if (ResourceGroupModelData.CLASS_NAME.equals(record.getClassName()))
            {
                record.delete();
                model.getGroupStore().remove(record);
            }
        }
    }

    /**
     * When rename group is clicked, the rename group dialog is shown.
     */
    private void onRenameGroupClicked()
    {
        this.view.showRenameDialog();
    }

    /**
     * When rename group is confirmed, the selected record is saved to services with the name given by user.
     * 
     * @param event
     *            Rename group Confirmed event.
     */
    private void onRenameGroupConfirmed(AppEvent event)
    {
        String name = event.getData();
        List<RecordModelData> selection = this.view.getGroupSelection();
        RecordModelData record = selection.get(0);

        record.setName(name);
        record.save();

        model.getGroupStore().update(record);
    }

    /**
     * When create group is clicked, the create group dialog is shown.
     */
    private void onCreateGroupClicked()
    {
        this.view.displayCreateGroupDialog();
    }

    /**
     * When create group is confirmed, a new group, the name of which is the one given by user, is created. It is also
     * added to the store as a child of the selected node. The selected node id (group id) is set as parent id on the
     * newly created group.
     * 
     * @param event
     *            Create group confirmed event.
     */
    private void onCreateGroupConfirmed(AppEvent event)
    {
        String name = event.getData();
        List<RecordModelData> selection = this.view.getGroupSelection();
        RecordModelData record = selection.get(0);
        ResourceGroupModelData group = new ResourceGroupModelData(name);

        if (record.getId() != -1)
        {
            group.setParentId(record.getId());
        }
        group.save();
        model.getGroupStore().add(record, group, false);
    }

    /**
     * When refresh group is clicked, leaves of the selected group are reloaded and the group node is expanded.
     * 
     * @param event
     *            Refresh group event.
     */
    private void onGroupRefreshClicked(AppEvent event)
    {
        List<RecordModelData> selection = this.view.getGroupSelection();
        if (selection.size() > 0)
        {
            // this.model.getGroupStore().removeAll(selection.get(0));
            this.model.getGroupTreeLoader().loadChildren(selection.get(0));
            this.view.expand(selection.get(0));
        }
    }

    /**
     * When groups are loaded for the first time, root node is expanded.
     * 
     * @param event
     *            First load event.
     */
    private void onFirstLoad(AppEvent event)
    {
        this.view.expand((RecordModelData) event.getData());
    }

    /**
     * When a person is dropped from the person list into the group tree, this person id is added to this person list.
     * 
     * @param event
     *            Person dropped event.
     */
    @SuppressWarnings("unchecked")
    private void onPersonDropped(AppEvent event)
    {
        RecordModelData target = event.getData(HumanResourcesEvents.EVENT_VAR_TARGET);
        List<PersonModelData> personList = event.getData(HumanResourcesEvents.EVENT_VAR_SOURCE);
        for (PersonModelData source : personList)
        {
            if (!source.getResourceGroupIds().contains(target.getId()))
            {
                source.getResourceGroupIds().add(target.getId());
                if (target.get(ResourceGroupModelData.RESOURCE_IDS_FIELD) == null)
                {
                    target.set(ResourceGroupModelData.RESOURCE_IDS_FIELD, new ArrayList<Long>());
                }

                ((List<Long>) target.get(ResourceGroupModelData.RESOURCE_IDS_FIELD)).add(source.getId());

                source.save();
            }
        }
    }

    /**
     * When an element is dropped, a specific behavior is adopted depending on the source and target classes.
     * 
     * @param event
     *            Element dropped event.
     */
    private void onGroupDropped(AppEvent event)
    {
        List<RecordModelData> source = event.getData(HumanResourcesEvents.EVENT_VAR_SOURCE);
        RecordModelData target = event.getData(HumanResourcesEvents.EVENT_VAR_TARGET);
        RecordModelData parent = event.getData(HumanResourcesEvents.EVENT_VAR_PARENT);

        if (source.isEmpty())
        {
            return;
        }

        if (ResourceGroupModelData.CLASS_NAME.equals(source.get(0).getClassName()))
        {
            if (ProjectModelData.CLASS_NAME.equals(target.getClassName()))
            {
                for (RecordModelData record : source)
                {
                    this.onGroupDroppedInProject((ResourceGroupModelData) record, (ProjectModelData) target, parent);
                }
            }
            else if (ResourceGroupModelData.CLASS_NAME.equals(target.getClassName()))
            {
                for (RecordModelData record : source)
                {
                    this.onGroupDroppedInGroup(record, target, parent);
                }
            }
        }
        else
        {
            for (RecordModelData record : source)
            {
                this.onPersonDroppedInGroup(record, target, parent);
            }
        }
    }

    /**
     * When a person is dropped in group, the person group links are updated : targeted group is linked to the person,
     * source group is unlinked.
     * 
     * @param source
     *            Dropped person.
     * @param target
     *            Targeted group.
     * @param parent
     *            Parent group.
     */
    private void onPersonDroppedInGroup(RecordModelData source, RecordModelData target, RecordModelData parent)
    {
        List<Long> groupIds = source.get(PersonModelData.RESOURCEGROUP_IDS_FIELD);
        if (groupIds != null)
        {
            groupIds.remove(parent.getId());
            groupIds.add(target.getId());
        }

        source.setSimpleClassName(PersonModelData.SIMPLE_CLASS_NAME);
        source.save();
    }

    /**
     * When a group is dropped, its parent id is updated with its new parent id. If it has no new parent, it set its
     * parent id to null.
     * 
     * @param source
     *            Dropped person.
     * @param target
     *            Targeted group.
     * @param parent
     *            Parent group.
     */
    private void onGroupDroppedInGroup(RecordModelData source, RecordModelData target, RecordModelData parent)
    {
        if (target.getId() != -1)
        {
            source.set(ResourceGroupModelData.PARENT_ID_FIELD, target.getId());
        }
        else
        {
            source.set(ResourceGroupModelData.PARENT_ID_FIELD, 0); // 0 means null for services.
        }
        source.save();
    }

    /**
     * When a group is dropped in a project, targeted project is linked to the source group.
     * 
     * 
     * @param source
     *            Dropped person.
     * @param target
     *            Targeted group.
     * @param parent
     *            Parent group.
     */
    private void onGroupDroppedInProject(ResourceGroupModelData source, ProjectModelData target, RecordModelData parent)
    {

        if (target.getId() != -1)
        {
            List<Long> resourcesIds = target.get(ProjectModelData.RESOURCEGROUP_IDS_FIELD);
            if (resourcesIds == null)
            {
                resourcesIds = new ArrayList<Long>();
                target.set(ProjectModelData.RESOURCEGROUP_IDS_FIELD, resourcesIds);
            }
            resourcesIds.add(source.getId());

            List<Long> projectIds = source.get(ResourceGroupModelData.PROJECT_IDS_FIELD);
            if (projectIds == null)
            {
                projectIds = new ArrayList<Long>();
                source.set(ResourceGroupModelData.PROJECT_IDS_FIELD, projectIds);
            }
            projectIds.add(target.getId());

            source.save();
        }

    }

    /**
     * When permissions are initialized, widgets are updated.
     */
    private void onPermissionsInitialized()
    {

    }

    /**
     * Register events supported by controller.
     */
    private void registerEvents()
    {
        this.registerEventTypes(HumanResourcesEvents.GROUP_PERSON_FILTER_CHANGED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_GROUP_DROPPED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_PERSON_DROPPED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_FIRST_LOAD);
        this.registerEventTypes(HumanResourcesEvents.GROUP_GROUP_REFRESH_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_CREATE_GROUP_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_CREATE_GROUP_CONFIRMED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_RENAME_GROUP_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_RENAME_GROUP_CONFIRMED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_DELETE_GROUP_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_MAKE_LEADER_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_REMOVE_PERSON_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_REMOVE_FROM_PROJECT_CLICKED);
        this.registerEventTypes(HumanResourcesEvents.GROUP_PROJECT_REFRESH_CLICKED);

        this.registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
    }
}
