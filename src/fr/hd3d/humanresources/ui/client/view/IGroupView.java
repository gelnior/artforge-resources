package fr.hd3d.humanresources.ui.client.view;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public interface IGroupView
{

    String getFilterValue();

    List<RecordModelData> getProjectSelection();

    List<RecordModelData> getGroupSelection();

    void expandProject(RecordModelData recordModelData);

    void refreshGroup(RecordModelData group);

    void showRenameDialog();

    void displayCreateGroupDialog();

    void expand(RecordModelData recordModelData);

}
