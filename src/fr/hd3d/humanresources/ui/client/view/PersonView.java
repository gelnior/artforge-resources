package fr.hd3d.humanresources.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.explorer.ExplorerRefreshButton;
import fr.hd3d.common.ui.client.widget.explorer.config.ExplorerConfig;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.AutoSaveButton;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.PrintButton;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.ShowMosaicToolItem;
import fr.hd3d.common.ui.client.widget.identitypanel.IdentityPanel;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationDialog;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.workobject.browser.view.WorkObjectView;
import fr.hd3d.humanresources.ui.client.constant.HumanResourcesConstants;
import fr.hd3d.humanresources.ui.client.controller.PersonController;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.images.ResourceImages;
import fr.hd3d.humanresources.ui.client.model.PersonModel;
import fr.hd3d.humanresources.ui.client.view.column.HumanResourcesColumnInfoProvider;
import fr.hd3d.humanresources.ui.client.view.widget.PersonDialog;


/**
 * View that lists persons and offer stuffs for edtion, creation and deletion.
 * 
 * @author HD3D
 */
public class PersonView extends WorkObjectView
{
    public static final String NAME = "person";
    /** Constant strings to display : dialog messages, button label... */
    public static HumanResourcesConstants CONSTANTS = GWT.create(HumanResourcesConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Model handling data */
    private final PersonModel model = new PersonModel();
    /** Controller that handles person view events. */
    private final PersonController controller = new PersonController(this, model);

    /** Dialog used to set relations between groups and persons. */
    private final RelationDialog relationDialog = new RelationDialog();
    /** Identity panel shows relations of the selected record (in the grid). */
    private final IdentityPanel identityPanel = new IdentityPanel();

    /** Button that shows new person dialog when clicked. */
    private ToolBarButton addPersonButton;
    /** Button that deletes selected persons form services. */
    private ToolBarButton deletePersonButton;
    /** Button that shows relation dialog for selection. */
    private ToolBarButton relationButton;

    /** Tool bar separator between person buttons and explorer buttons. */
    private SeparatorToolItem personSeparator;

    /** Tool bar button used to enable explorer auto saving. */
    private AutoSaveButton autoSaveButton;

    /** Dialog displayed when user ask for creating new person. */
    private final PersonDialog personDialog = new PersonDialog();

    /**
     * Default constructor. Creates all widgets, sets controller and forward PERMISSION_INITIALIEZD event to the
     * controller.
     */
    public PersonView()
    {
        super();

        this.setSheetCombo();

        this.setPersonIdentityPanel();
        this.setHtmlIds();

        this.relationDialog.addSpecificallyExlcudedField(PersonModelData.PROJECT_IDS_FIELD);

        this.controller.addChild(this.explorer.getController());
        this.controller.handleEvent(new AppEvent(CommonEvents.PERMISSION_INITIALIZED));

        Boolean isAutoSave = UserSettings.getSettingAsBoolean(ExplorerConfig.SETTING_AUTOSAVE);
        if (isAutoSave != null)
            ExplorerController.autoSave = isAutoSave;
        if (ExplorerController.autoSave)
            this.toggleAutoSave();

        this.configureSheetEditorWindow();
    }

    @Override
    public void setWestPanel()
    {}

    /**
     * @return view controller
     */
    public MaskableController getController()
    {
        return this.controller;
    }

    /**
     * Refresh navigation tree for given project.
     * 
     * @param project
     *            The project used to set data inside tree /** Idle every widget contains inside view and mask view
     *            controller.
     */
    @Override
    public void idle()
    {
        super.idle();
        this.controller.mask();
    }

    /**
     * Unidle every widget contains inside view and unmask view controller.
     */
    @Override
    public void unidle()
    {
        super.unidle();
        this.controller.unMask();
    }

    /**
     * Mask explorer grid.
     */
    public void maskExplorer()
    {
        this.explorer.showLoading();
    }

    /** Configure sheet combo box and register its store to model. */
    private void setSheetCombo()
    {
        this.sheetCombo.setSelectionChangedEvent(HumanResourcesEvents.PERSON_SHEET_CHANGED);
        this.sheetCombo.addAvailableEntity(PersonModelData.SIMPLE_CLASS_NAME);
        this.model.setSheetStore(this.sheetCombo.getServiceStore());
    }

    /**
     * Load sheets for the first time.
     */
    public void initSheets()
    {
        if (this.model.getSheetStore().getModels().isEmpty())
        {
            this.model.getSheetStore().reload();
        }
    }

    /**
     * Set active view in explorer.
     * 
     * @param view
     *            The view to set.
     */
    @Override
    public void setExplorerView(SheetModelData view)
    {
        this.explorer.setCurrentSheet(view);
    }

    /**
     * @return Model selected inside dialog explorer.
     */
    public List<Hd3dModelData> getSelectedModels()
    {
        return this.explorer.getSelectedModels();
    }

    /**
     * Enable or disable view ComboBox
     * 
     * @param enabled
     *            True if the combo box must be enabled, false either.
     */
    @Override
    public void enableViewComboBox(boolean enabled)
    {
        this.sheetCombo.setEnabled(enabled);
    }

    /**
     * Select first sheet contained in the sheet combobox.
     */
    @Override
    public void selectFirstSheet()
    {
        this.sheetCombo.selectFirstSheet();
    }

    /**
     * @return Currently selected sheet.
     */
    @Override
    public SheetModelData getSelectedSheet()
    {
        return this.sheetCombo.getValue();
    }

    /**
     * Show saving indicator.
     */
    @Override
    public void showSaving()
    {
        this.status.show();
    }

    /**
     * Hide saving indicator.
     */
    @Override
    public void hideSaving()
    {
        this.status.hide();
    }

    public void displayForm(AppEvent event)
    {
        personDialog.show();
    }

    /**
     * Refresh identity panel data with person data whose ID is equal to <i>id</i>.
     * 
     * @param id
     */
    public void refreshIdentityPanel(Long id)
    {
        if (id != null)
        {
            this.identityPanel.refreshIdentityData(id);
        }
        else
        {
            this.identityPanel.refreshIdentityData();
        }
    }

    /**
     * Clear identity panel data.
     */
    public void clearIdentityPanel()
    {
        this.identityPanel.clearData();
    }

    /**
     * Show relation tool for managing links between group and persons.
     * 
     * @param selection
     *            Selection concerned by group linking.
     */
    public void showRelationTool(List<Hd3dModelData> selection)
    {
        if (selection.size() > 0)
        {
            this.relationDialog.show(selection);
        }
    }

    /**
     * @param key
     *            The key of the value to get.
     * @return Value corresponding to "key" in the favorite cookie string.
     */
    public String getFavCookieValue(String key)
    {
        return FavoriteCookie.getFavParameterValue(key);
    }

    /**
     * Sets a new key-value couple in the favorite cookie string.
     * 
     * @param key
     *            The key of the value to set.
     * @param value
     *            The value to set.
     */
    public void putFavCookieValue(String key, String value)
    {
        FavoriteCookie.putFavValue(key, value);
    }

    /**
     * Set create person button visible or not.
     * 
     * @param visible
     *            True to set it visible.
     */
    public void setCreatePersonToolITemVisible(boolean visible)
    {
        this.addPersonButton.setVisible(visible);
    }

    /**
     * Set edit links button visible or not.
     * 
     * @param visible
     *            True to set it visible.
     */
    public void setRelationToolITemVisible(boolean visible)
    {
        this.relationButton.setVisible(visible);

        if (!visible && !addPersonButton.isVisible())
        {
            personSeparator.setVisible(false);
        }
        else
        {
            personSeparator.setVisible(true);
        }
    }

    /**
     * Set save sheet button visible or not.
     * 
     * @param visible
     *            True to set it visible.
     */
    public void setSaveToolITemVisible(boolean visible)
    {
        this.saveButton.setVisible(visible);
    }

    /**
     * Set delete person button visible or not.
     * 
     * @param visible
     *            True to set it visible.
     */
    public void enablePersonDeleteButton(boolean enabled)
    {
        this.deletePersonButton.setEnabled(enabled);
    }

    /**
     * Open a dialog box asking for a confirmation of selection deletion.
     */
    public void showDeletePersonConfirmation()
    {
        MessageBox.confirm(COMMON_CONSTANTS.Confirm(), CONSTANTS.AreYouSureToDelete(), new Listener<MessageBoxEvent>() {
            public void handleEvent(MessageBoxEvent be)
            {
                if (be.getButtonClicked().getItemId() == "yes")
                {
                    EventDispatcher.forwardEvent(HumanResourcesEvents.DELETE_PERSON_CONFIRMED);
                }
            }
        });
    }

    /**
     * Sets available entities in sheet editor window.
     */
    private void configureSheetEditorWindow()
    {
        List<EntityModelData> entities = new ArrayList<EntityModelData>();

        EntityModelData entity = new EntityModelData();
        entity.setName("Person");
        entity.setClassName(PersonModelData.SIMPLE_CLASS_NAME);
        entities.add(entity);

        this.model.setEntities(entities);
    }

    /**
     * Put all tool items inside tool bar.
     */
    @Override
    public void setExplorerToolBar()
    {
        explorerBar.setStyleAttribute("padding", "5px");

        explorerBar.add(sheetCombo);
        explorerBar.add(filterPanelButton);
        explorerBar.add(sheetFilterComboBox);

        sheetFilterComboBox.registerToggleButton(filterPanelButton);
        CSSUtils.set1pxBorder(filterPanelButton, "transparent");
        explorerBar.add(new SeparatorToolItem());

        addPersonButton = new ToolBarButton(ResourceImages.getPersonIcon(), CONSTANTS.AddNewPerson(),
                HumanResourcesEvents.SHOW_NEW_PERSON_WINDOW);
        /** Button that deletes selected persons form services. */
        deletePersonButton = new ToolBarButton(Hd3dImages.getDeleteIcon(), CONSTANTS.DeleteSelectedPeople(),
                HumanResourcesEvents.DELETE_PERSON_CLICKED);
        /** Button that shows relation dialog for selection. */
        relationButton = new ToolBarButton(ResourceImages.getPersonLinkIcon(), CONSTANTS.AddPersonToGroups(),
                HumanResourcesEvents.RELATION_PERSON_GROUP_CLICKED);
        /** Tool bar separator between person buttons and explorer buttons. */
        personSeparator = new SeparatorToolItem();
        /** Tool bar indicator used to show that explorer data saving is not finished. */
        /** Tool bar button used to enable explorer auto saving. */
        autoSaveButton = new AutoSaveButton();

        /** Dialog displayed when user ask for creating new person. */

        explorerBar.add(addPersonButton);
        explorerBar.add(relationButton);
        explorerBar.add(deletePersonButton);
        explorerBar.add(personSeparator);

        explorerBar.add(csvExportButton);
        explorerBar.add(new PrintButton());
        explorerBar.add(new ShowMosaicToolItem());
        explorerBar.add(new SeparatorToolItem());
        explorerBar.add(new ExplorerRefreshButton(this.explorer));
        explorerBar.add(autoSaveButton);
        explorerBar.add(saveButton);
        explorerBar.add(status);

        explorerBar.add(new FillToolItem());
        explorerBar.add(newSheetToolItem);
        explorerBar.add(editSheetToolItem);
        explorerBar.add(deleteSheetToolItem);

        explorer.setTopComponent(explorerBar);
        this.explorer.setColumnInfoProvider(new HumanResourcesColumnInfoProvider());
    }

    /**
     * Set identity panel to the east.
     */
    private void setPersonIdentityPanel()
    {
        identityPanel.buildFields(PersonModelData.SIMPLE_CLASS_NAME);
        this.addEast(identityPanel);
    }

    /**
     * Set HTML IDs to facilitate Selenium testings.
     */
    private void setHtmlIds()
    {
        this.sheetCombo.setId("sheet-combo");
        this.addPersonButton.setId("add-person-button");
        this.deletePersonButton.setId("delete-person-button");
        this.saveButton.setId("save-button");
    }

    /**
     * Refresh explorer data for columns described in <i>sheet</i>.
     * 
     * @param sheet
     *            The sheet to load.
     */
    public void refreshExplorerData(SheetModelData sheet)
    {
        this.explorer.setCurrentSheet(sheet);
        if (sheet != null)
            this.sheetFilterComboBox.refreshFilterList(sheet);
        else
        {
            this.sheetFilterComboBox.clear();
            this.sheetFilterComboBox.getStore().removeAll();
        }
    }

    public void toggleAutoSave()
    {
        this.autoSaveButton.toggle();
    }
}
