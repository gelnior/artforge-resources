package fr.hd3d.humanresources.ui.client.view.tree;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.PersonReader;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReader;
import fr.hd3d.common.ui.client.modeldata.reader.ResourceGroupReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTree;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataModel;
import fr.hd3d.humanresources.ui.client.constant.HumanResourcesConstants;
import fr.hd3d.humanresources.ui.client.model.loader.GroupTreeDataLoader;


/**
 * Tree displaying group associated to projects.
 * 
 * @author HD3D
 */
public class GroupTree extends BaseTree<PersonModelData, ResourceGroupModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static HumanResourcesConstants CONSTANTS = GWT.create(HumanResourcesConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default constructor (set root name and appropriate data reader).
     */
    public GroupTree()
    {
        super(COMMON_CONSTANTS.Groups(), new PersonReader(), new ResourceGroupReader());
    }

    /**
     * Configure data loader.
     * 
     * @see fr.hd3d.common.ui.client.widget.basetree.BaseTree#setModel(java.lang.String,
     *      fr.hd3d.common.ui.client.modeldata.reader.IReader, fr.hd3d.common.ui.client.modeldata.reader.IReader)
     */
    @Override
    protected void setModel(String rootName, IReader<PersonModelData> leafReader,
            IReader<ResourceGroupModelData> parentReader)
    {
        TreeServicesProxy<RecordModelData> proxy = new TreeServicesProxy<RecordModelData>(new RecordReader());
        GroupTreeDataLoader loader = new GroupTreeDataLoader(proxy, leafReader, parentReader);
        this.model = new BaseTreeDataModel<PersonModelData, ResourceGroupModelData>(loader);
        this.model.setRootName(rootName);
    }
}
