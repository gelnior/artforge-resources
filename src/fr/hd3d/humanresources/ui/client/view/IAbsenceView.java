package fr.hd3d.humanresources.ui.client.view;

import java.util.Date;
import java.util.List;

import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.humanresources.ui.client.model.modeldata.AbsenceRowModelData;


public interface IAbsenceView
{

    Date getEndDate();

    Date getStartDate();

    void rebuildGrid();

    List<AbsenceRowModelData> getSelection();

    void showEventEditor();

    void showAbsenceEditor();

    ResourceGroupModelData getCurrentGroup();

    void maskGrid();

    void unMaskGrid();

    void setGroupComboEnabled(boolean enabled);

    void setDatePickerEnabled(boolean enabled);

    void setRefreshEnabled(boolean hasReadRights);

    void setEventButtonEnabled(boolean hasReadRights);

    void setAbsenceButtonEnabled(boolean hasReadRights);

    void initEditorPermissions();

    List<AbsenceModelData> getAbsencesFromEditor();

    AbsenceRowModelData getAbsenceEditorSelection();

}
