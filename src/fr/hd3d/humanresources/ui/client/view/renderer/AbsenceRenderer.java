package fr.hd3d.humanresources.ui.client.view.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.humanresources.ui.client.model.modeldata.AbsenceRowModelData;


/**
 * Absence Renderer display colors for absence value : Red for 'A' (absence), Gray for 'W' (week-end), Green for 'E'
 * (event) and no color for 'P' (presence);
 * 
 * @author HD3D
 */
public class AbsenceRenderer implements GridCellRenderer<AbsenceRowModelData>
{
    /** CSS Red Color. */
    public static final String RED = "red";
    /** CSS Gray Color. */
    public static final String GRAY = "#DDD";
    /** CSS Green Color. */
    public static final String GREEN = "#CFC";

    public Object render(AbsenceRowModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<AbsenceRowModelData> store, Grid<AbsenceRowModelData> grid)
    {
        char val = model.get(property);

        if (val == AbsenceRowModelData.PRESENCE)
        {
            return "";
        }
        else if (val == AbsenceRowModelData.EVENT)
        {
            return getDivValue(GREEN);
        }
        else if (val == AbsenceRowModelData.WEEKEND)
        {
            return getDivValue(GRAY);
        }
        else
        {
            return getDivValue(RED);
        }
    }

    private String getDivValue(String color)
    {
        return "<div style=\"background-color: " + color + "; witdth: 100%; height: 100%;\">&nbsp;</div>";
    }
}
