package fr.hd3d.humanresources.ui.client.view.renderer;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.EContractType;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ContractTypeMap;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorateurColumnContextMenu;


public class ContractTypeRenderer<M extends Hd3dModelData> implements GridCellRenderer<M>,
        IExplorateurColumnContextMenu<Hd3dModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);
    
    private ContractTypeMap map = new ContractTypeMap();

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return getStatusRenderedValue(obj);
    }

    public String render(GroupColumnData data)
    {
        return getStatusRenderedValue(data.gvalue);
    }

    private String getStatusRenderedValue(Object gValue)
    {
        String stringValue = "";
        if (gValue != null)
        {
            stringValue = (String) gValue;
            
            if (stringValue.equals(EContractType.CDI.toString()))
            {
                stringValue = "<div style='padding: 3px; white-space: normal; font-weight: bold; text-align:center; background-color:"
                        + ContractTypeMap.getColorForStatus(EContractType.CDI.toString()) + ";'>" + map.get(EContractType.CDI.toString()).getName() + "</div>";
            }
            else if (stringValue.equals(EContractType.CDD.toString()))
            {
                stringValue = "<div style=' padding: 3px;  white-space: normal; font-weight: bold ; text-align:center; background-color:"
                        + ContractTypeMap.getColorForStatus(EContractType.CDD.toString()) + ";'>" + map.get(EContractType.CDD.toString()).getName() + "</div>";
            }
            else if (stringValue.equals(EContractType.INTERIM.toString()))
            {
                stringValue = "<div style=' padding: 3px;  white-space: normal; font-weight: bold ; text-align:center; background-color:"
                        +ContractTypeMap.getColorForStatus(EContractType.INTERIM.toString()) + ";'>" + map.get(EContractType.INTERIM.toString()).getName()+ "</div>";
            }
            else if (stringValue.equals(EContractType.FREELANCE.toString()))
            {
                stringValue = "<div style=' padding: 3px;  white-space: normal; font-weight: bold ; text-align:center; background-color:"
                        + ContractTypeMap.getColorForStatus(EContractType.FREELANCE.toString()) + ";'>" + map.get(EContractType.FREELANCE.toString()).getName() + "</div>";
            }
            else if (stringValue.equals(EContractType.INTERMITTENT.toString()))
            {
                stringValue = "<div style=' padding: 3px;  white-space: normal; font-weight: bold ; text-align:center; background-color:"
                        + ContractTypeMap.getColorForStatus(EContractType.INTERMITTENT.toString()) + ";'>" + map.get(EContractType.INTERMITTENT.toString()).getName() + "</div>";
            }
            else if (stringValue.equals(EContractType.UNKNOWN.toString()))
            {
                stringValue = "<div style=' padding: 3px;  white-space: normal; font-weight: bold ; text-align:center; background-color:"
                        + ContractTypeMap.getColorForStatus(EContractType.UNKNOWN.toString()) + ";'>" + map.get(EContractType.UNKNOWN.toString()).getName() + "</div>";
            }
        }
        else
        {
            stringValue += "unknow,";
        }
        return stringValue;
    }

  

    public void handleGridEvent(GridEvent<Hd3dModelData> ge)
    {
        if (ge.getType() == Events.ContextMenu)
        {
            
        }
    }
}
