package fr.hd3d.humanresources.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ContractModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.dialog.ConfirmationDisplayer;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.PrintButton;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.workobject.browser.events.WorkObjectEvents;
import fr.hd3d.common.ui.client.widget.workobject.browser.view.WorkObjectView;
import fr.hd3d.humanresources.ui.client.constant.HumanResourcesConstants;
import fr.hd3d.humanresources.ui.client.controller.ContractController;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.images.ResourceImages;
import fr.hd3d.humanresources.ui.client.model.ContractModel;
import fr.hd3d.humanresources.ui.client.view.column.HumanResourcesColumnInfoProvider;
import fr.hd3d.humanresources.ui.client.view.widget.ContractAmendmentDialog;
import fr.hd3d.humanresources.ui.client.view.widget.ContractDialog;


/**
 * View that lists contract and offer stuffs for edtion, creation and deletion.
 * 
 * @author HD3D
 */
public class ContractView extends WorkObjectView
{
    /** View name to tab distinction purpose. */
    public static final String NAME = "contract";

    /** Constant strings to display : dialog messages, button label... */
    public static HumanResourcesConstants CONSTANTS = GWT.create(HumanResourcesConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Model handling data */
    private final ContractModel model = new ContractModel();
    /** Controller that handles contract view events. */
    private final ContractController controller = new ContractController(this, model);

    /** Dialog displayed when user ask for creating new person. */
    public final ContractDialog contractDialog = new ContractDialog();
    /** Dialog displayed when user ask for creating new person. */
    public final ContractAmendmentDialog contractAmendmentDialog = new ContractAmendmentDialog();
    /**
     * Default constructor. Creates all widgets, sets controller and forward PERMISSION_INITIALIEZD event to the
     * controller.
     */
    public ContractView()
    {
        super();

        this.setSheetCombo();
        
        this.configureExplorer();

        //this.controller.addChild(this.identityPortal.getController());
        this.controller.addChild(this.explorer.getController());
    }
    
    @Override
    public void setExplorerToolBar()
    {
        explorerBar.setStyleAttribute("padding", "5px");

        explorerBar.add(sheetCombo);
        explorerBar.add(filterPanelButton);
        explorerBar.add(sheetFilterComboBox);
        sheetFilterComboBox.registerToggleButton(filterPanelButton);
        CSSUtils.set1pxBorder(filterPanelButton, "transparent");
        explorerBar.add(new SeparatorToolItem());

        ToolBarButton addContractButton = new ToolBarButton(ExplorateurCSS.ADD_ROW_ICON_CLASS,
                CONSTANTS.AddNewContract(), HumanResourcesEvents.NEW_CONTRACT_CLICKED);
        
        ToolBarButton addContractAmendmentButton = new ToolBarButton(ResourceImages.getContractAmendmentIcon(),
                CONSTANTS.AddNewContractAmendment(), HumanResourcesEvents.NEW_CONTRACT_AMENDMENT_CLICKED);
        
        /** Button that deletes selected persons form services. */
        ToolBarButton deleteContractButton = new ToolBarButton(ExplorateurCSS.DELETE_ROW_ICON_CLASS,
                CONSTANTS.DeleteSelectedContract(), HumanResourcesEvents.DELETE_CONTRACT_CLICKED);
        explorerBar.add(addContractButton);
        explorerBar.add(addContractAmendmentButton);
        explorerBar.add(deleteContractButton);
        //explorerBar.add(new ShowMosaicToolItem());
        explorerBar.add(new SeparatorToolItem());
        explorerBar.add(new PrintButton());
        explorerBar.add(csvExportButton);
        explorerBar.add(new SeparatorToolItem());
        explorerBar.add(saveButton);
        explorerBar.add(status);

        explorerBar.add(new FillToolItem());
        explorerBar.add(newSheetToolItem);
        explorerBar.add(editSheetToolItem);
        explorerBar.add(deleteSheetToolItem);

        explorer.setTopComponent(explorerBar);
    }

    @Override
    public void setWestPanel()
    {}

    /**
     * @return view controller
     */
    public MaskableController getController()
    {
        return this.controller;
    }

    /**
     * Refresh navigation tree for given project.
     * 
     * @param project
     *            The project used to set data inside tree /** Idle every widget contains inside view and mask view
     *            controller.
     */
    @Override
    public void idle()
    {
        super.idle();
        this.controller.mask();
    }

    /**
     * Unidle every widget contains inside view and unmask view controller.
     */
    @Override
    public void unidle()
    {
        super.unidle();
        this.controller.unMask();
    }

    public void initSheets()
    {
        if (this.model.getSheetStore().getModels().isEmpty())
        {
            this.model.getSheetStore().reload();
        }
    }

    /** Configure sheet combo box and register its store to model. */
    private void setSheetCombo()
    {
        this.model.setSheetStore(this.sheetCombo.getServiceStore());
       // this.sheetCombo.setType(ESheetType.TEAM);
        this.sheetCombo.addAvailableEntity(ContractModelData.SIMPLE_CLASS_NAME);
        this.sheetCombo.addSelectionChangedListener(new EventSelectionChangedListener<SheetModelData>(
                WorkObjectEvents.SHEET_CHANGED));
    }

    /** Configure explorer and sheet editor for Person browsing. */
    private void configureExplorer()
    {
        List<EntityModelData> entities = new ArrayList<EntityModelData>();
        EntityModelData entity = new EntityModelData();
        
        String entityName = ContractModelData.SIMPLE_CLASS_NAME;
        entity.setName(ServicesField.getHumanName(entityName));
        entity.setClassName(entityName);
        entities.add(entity);
        
        this.explorer.setColumnInfoProvider(new HumanResourcesColumnInfoProvider());   
        this.model.setExplorerStore(explorer.getStore(), ContractModelData.SIMPLE_CLASS_NAME.toLowerCase());
        
        this.model.setEntities(entities);
        this.explorer.setApplicationFilter(this.model.getFilter());
        
        //OrderBy orderBy1 = new OrderBy(ContractModelData.PERSON_NAME_FIELD);
        OrderBy orderBy2 = new OrderBy(ContractModelData.START_DATE_FIELD);
        //this.explorer.addParameter(orderBy1);
        this.explorer.addParameter(orderBy2);
        //this.explorer.getStore().sort(ContractModelData.PERSON_NAME_FIELD, SortDir.ASC);
    }
    
    public void displayForm(AppEvent event)
    {
        contractDialog.show();
    }
    
    public void displayFormAmendment(AppEvent event)
    {
        Hd3dModelData contract = (Hd3dModelData) (this.explorer.getSelectedModels().get(0));
        contractAmendmentDialog.setContract(contract);
        contractAmendmentDialog.show();
    }
    
    /**
     * Show delete contract delete confirmation box.
     */
    public void showDeleteContractConfirmation()
    {
        ConfirmationDisplayer
                .display("Contract deletion", "Are you sure you want to delete selected contracts ?",
                        HumanResourcesEvents.DELETE_CONTRACT_CONFIRMED);
    }

}
