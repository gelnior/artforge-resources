package fr.hd3d.humanresources.ui.client.view;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.EventBaseListener;
import fr.hd3d.common.ui.client.widget.mainview.MainView;
import fr.hd3d.humanresources.ui.client.config.HumanResourceConfig;
import fr.hd3d.humanresources.ui.client.constant.HumanResourcesConstants;
import fr.hd3d.humanresources.ui.client.controller.HumanResourcesController;
import fr.hd3d.humanresources.ui.client.error.HumanResourcesErrors;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.model.HumanResourcesModel;


/**
 * This is the main view. It contains a tab panel. Each tab is dedicated to a sub application. There are tabs for :
 * person listing, groups management and absence recording.
 * 
 * @author HD3D
 */
public class HumanResourcesView extends MainView
{
    /** Constant strings to display : dialog messages, button label... */
    public static HumanResourcesConstants CONSTANTS = GWT.create(HumanResourcesConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Panel set into the viewport. */
    private TabPanel mainPanel = new TabPanel();

    /** The panel hosting the application. */
    private final ContentPanel panel = new ContentPanel();

    private final TabItem contractTabItem = new TabItem(COMMON_CONSTANTS.Contracts());
    private final TabItem personTabItem = new TabItem(COMMON_CONSTANTS.Persons());

    /** Panel for recording person absences and events. */
    private AbsenceView absencePanel;
    /** Panel for managing links between groups, persons and projects */
    private GroupView groupPanel;
    /** Panel for person listing. */
    private PersonView personPanel;
    /** Panel for contract listing. */
    // private ContractView contractPanel;

    /** Controller that handles main events. */
    private HumanResourcesController controller;
    /** Model handling application data. */
    private final HumanResourcesModel model = new HumanResourcesModel();

    /** Default constructor, sets layout and sets controller. */
    public HumanResourcesView()
    {
        super(CONSTANTS.Teams());
    }

    /** Registers all controller. */
    @Override
    public void init()
    {
        controller = new HumanResourcesController(this, model);
        EventDispatcher.get().addController(controller);
    }

    /**
     * Initialize all widgets (one tab for each panel) and add them to the viewport.
     */
    public void initWidgets()
    {
        personPanel = new PersonView();
        absencePanel = new AbsenceView();
        groupPanel = new GroupView();

        mainPanel = new TabPanel();

        TabItem absenceTabItem = new TabItem(CONSTANTS.Absence());
        TabItem groupTabItem = new TabItem(COMMON_CONSTANTS.Groups());
        personTabItem.addListener(Events.Select, new EventBaseListener(HumanResourcesEvents.PERSON_TAB_CLICKED));

        personTabItem.setLayout(new FitLayout());
        absenceTabItem.setLayout(new FitLayout());
        groupTabItem.setLayout(new FitLayout());

        personTabItem.add(personPanel);
        absenceTabItem.add(absencePanel);
        groupTabItem.add(groupPanel);

        // contractPanel = new ContractView();
        // contractTabItem.setLayout(new FitLayout());
        // contractTabItem.add(contractPanel);
        // contractTabItem.addListener(Events.Select, new EventBaseListener(HumanResourcesEvents.CONTRACT_TAB_CLICKED));
        // this.contractPanel.idle();

        mainPanel.add(personTabItem);
        mainPanel.add(groupTabItem);
        mainPanel.add(absenceTabItem);
        // mainPanel.add(contractTabItem);

        this.panel.setLayout(new FitLayout());
        this.panel.setHeaderVisible(false);
        this.panel.setBorders(false);
        this.panel.setBodyBorder(false);
        this.panel.add(mainPanel);
        this.addToViewport(panel);

        RootPanel.get().add(loadingPanel);

    }

    /**
     * Display error message depending on error code transmitted.
     * 
     * @see fr.hd3d.common.ui.client.widget.mainview.MainView#displayErrorMessageBox(int,
     *      com.extjs.gxt.ui.client.event.Listener)
     */
    @Override
    protected void displayErrorMessageBox(int error, String userMsg, String stack, Listener<MessageBoxEvent> callback)
    {
        super.displayErrorMessageBox(error, userMsg, stack, callback);
        switch (error)
        {
            case HumanResourcesErrors.NO_SELECTION:
                MessageBox.alert(COMMON_CONSTANTS.Error(), CONSTANTS.NoRecordSelected(), callback);
                break;
            case HumanResourcesErrors.TOO_BIG_SELECTION:
                MessageBox.alert(COMMON_CONSTANTS.Error(), CONSTANTS.OneRecordSelectedMax(), callback);
                break;
            default:
                ;
        }
    }

    /**
     * Make the person view inactive (to not react to event from similar view such as shot view).
     */
    public void idlePersonView()
    {
        this.personPanel.idle();
    }

    /**
     * Make the person view active.
     */
    public void unidlePersonView()
    {
        this.personPanel.unidle();
    }

    /**
     * Make the contract view inactive (to not react to event from similar view such as contract view).
     */
    public void idleContractView()
    {
        // this.contractPanel.idle();
    }

    /**
     * Make the contract view active.
     */
    public void unidleContractView()
    {
        // this.contractPanel.unidle();
    }

    public void initContractSheets()
    {
        // this.contractPanel.initSheets();
    }

    public void initPersonSheets()
    {
        this.personPanel.initSheets();
    }

    /**
     * Save selected tab name to favorite cookie.
     * 
     * @param tabName
     *            The clicked tab name.
     */
    public void saveTabToCookie(String tabName)
    {
        FavoriteCookie.putFavValue(HumanResourceConfig.TAB_COOKIE_VAR, tabName);
    }

    // public ContractView getContractPanel()
    // {
    // return contractPanel;
    // }

    public PersonView getPersonPanel()
    {
        return personPanel;
    }

}
