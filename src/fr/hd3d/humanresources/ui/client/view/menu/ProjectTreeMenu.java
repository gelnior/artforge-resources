package fr.hd3d.humanresources.ui.client.view.menu;

import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.util.TextMetrics;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;


/**
 * Context menu for project tree, provides remove group and refresh project functionality.
 * 
 * @author HD3D
 */
public class ProjectTreeMenu extends EasyMenu
{
    /** Remove group item. */
    private final MenuItem remove;
    /** Refresh group item. */
    private final MenuItem refresh;

    /** Project tree on which menu is set. */
    private final TreePanel<RecordModelData> projectTree;

    /**
     * Default constructor. Initializes menu items.
     * 
     * @param projectTree
     *            Project tree on which menu is set.
     */
    public ProjectTreeMenu(TreePanel<RecordModelData> projectTree)
    {
        this.projectTree = projectTree;

        this.remove = makeNewItem("Remove from Project", HumanResourcesEvents.GROUP_REMOVE_FROM_PROJECT_CLICKED,
                Hd3dImages.getDeleteIcon());
        this.refresh = makeNewItem("Refresh Project", HumanResourcesEvents.GROUP_PROJECT_REFRESH_CLICKED, Hd3dImages
                .getRefreshIcon());
    }

    /**
     * Dynamically updates the menu items depending on elements selected : "remove from project" is set when a group is
     * selelcted and "refresh" is set when a project is selected.
     */
    @Override
    protected void onBeforeShow(BaseEvent be)
    {
        List<RecordModelData> selection = projectTree.getSelectionModel().getSelectedItems();

        removeAll();
        if (selection.size() == 1)
        {
            RecordModelData record = selection.get(0);

            if (ProjectModelData.CLASS_NAME.equals(record.getClassName()))
            {
                add(refresh);

                setWidth(TextMetrics.get().getWidth(refresh.getText()) + 30);
            }
            else
            {
                if (record.getUserCanUpdate())
                    add(remove);
                setWidth(TextMetrics.get().getWidth(remove.getText()) + 30);
            }
        }
        else
        {
            if (PermissionUtil.canDeleteModelList(selection))
                add(remove);

            setWidth(TextMetrics.get().getWidth(remove.getText()) + 30);
        }
    }
}
