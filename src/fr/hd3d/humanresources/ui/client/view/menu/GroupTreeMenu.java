package fr.hd3d.humanresources.ui.client.view.menu;

import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.util.TextMetrics;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.humanresources.ui.client.constant.HumanResourcesConstants;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.images.ResourceImages;
import fr.hd3d.humanresources.ui.client.view.tree.GroupTree;


/**
 * The group tree menu.
 * 
 * @author HD3D
 */
public class GroupTreeMenu extends EasyMenu
{
    /** Constant strings to display : dialog messages, button label... */
    public static HumanResourcesConstants CONSTANTS = GWT.create(HumanResourcesConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    private final MenuItem insert;
    private final MenuItem remove;
    private final MenuItem rename;
    private final MenuItem doLeader;
    private final MenuItem removePerson;
    private final MenuItem refresh;

    /** Tree on which menu is set. */
    private final GroupTree groupTree;

    /**
     * Default constructor : initializes items and permissions.
     * 
     * @param groupTree
     *            Tree on which menu is set.
     */
    public GroupTreeMenu(GroupTree groupTree)
    {
        this.groupTree = groupTree;

        this.insert = makeNewItem(CONSTANTS.CreateGroup(), HumanResourcesEvents.GROUP_CREATE_GROUP_CLICKED, Hd3dImages
                .getAddIcon());
        this.remove = makeNewItem(CONSTANTS.DeleteGroup(), HumanResourcesEvents.GROUP_DELETE_GROUP_CLICKED, Hd3dImages
                .getDeleteIcon());
        this.rename = makeNewItem(CONSTANTS.RenameGroup(), HumanResourcesEvents.GROUP_RENAME_GROUP_CLICKED, Hd3dImages
                .getEditIcon());
        this.doLeader = makeNewItem(CONSTANTS.MakeLeader(), HumanResourcesEvents.GROUP_MAKE_LEADER_CLICKED,
                ResourceImages.getLeaderIcon());
        this.removePerson = makeNewItem(CONSTANTS.RemoveFromGroup(), HumanResourcesEvents.GROUP_REMOVE_PERSON_CLICKED,
                Hd3dImages.getDeleteIcon());
        this.refresh = makeNewItem(CONSTANTS.RefreshGroup(), HumanResourcesEvents.GROUP_GROUP_REFRESH_CLICKED,
                Hd3dImages.getRefreshIcon());

        this.insert.setEnabled(PermissionUtil.hasCreateRights(ResourceGroupModelData.SIMPLE_CLASS_NAME));
    }

    /**
     * Dynamically updates the menu items depending on elements selected.
     */
    @Override
    protected void onBeforeShow(BaseEvent be)
    {
        List<RecordModelData> selection = groupTree.getSelection();

        removeAll();
        if (selection.size() == 1)
        {
            RecordModelData record = selection.get(0);

            if (PersonModelData.CLASS_NAME.equals(record.getClassName()))
            {
                RecordModelData parent = groupTree.getStore().getParent(record);
                if (parent != null && parent.getUserCanUpdate())
                {
                    add(doLeader);
                }

                if (record.getUserCanUpdate())
                    add(removePerson);
                setWidth(TextMetrics.get().getWidth(removePerson.getText()) + 30);

            }
            else
            {
                if (PermissionUtil.hasCreateRights(ResourceGroupModelData.SIMPLE_CLASS_NAME))
                    add(insert);
                if (record.getUserCanUpdate())
                    add(rename);
                if (record.getUserCanDelete())
                    add(remove);
                add(refresh);
            }
        }
        else
        {
            if (PermissionUtil.canDeleteModelList(selection))
                add(remove);
        }
    }
}
