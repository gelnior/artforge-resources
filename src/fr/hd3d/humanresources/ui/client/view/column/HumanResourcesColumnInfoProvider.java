package fr.hd3d.humanresources.ui.client.view.column;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.model.BaseColumnInfoProvider;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.ThumbnailRenderer;
import fr.hd3d.common.ui.client.widget.grid.editor.FieldComboBoxEditor;
import fr.hd3d.humanresources.ui.client.view.renderer.ContractTypeRenderer;
import fr.hd3d.humanresources.ui.client.view.widget.ContractTypeComboBox;


/**
 * Provides appropriate cell renderer for a given renderer name
 * 
 * @author HD3D
 */
public class HumanResourcesColumnInfoProvider extends BaseColumnInfoProvider
{
    /**
     * Default constructor.
     */
    public HumanResourcesColumnInfoProvider()
    {
        super();
    }

    /**
     * @return Appropriate cell renderer for a given renderer name.
     * 
     * @see fr.hd3d.common.ui.client.widget.explorer.model.ItemInfoProvider#getRenderer(java.lang.String)
     */
    @Override
    public GridCellRenderer<Hd3dModelData> getRenderer(String name)
    {
        if (name == null)
        {
            return null;
        }
        else if (name.equals(Renderer.THUMBNAIL))
        {
            return new ThumbnailRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.CONTRACT_TYPE))
        {
            return new ContractTypeRenderer<Hd3dModelData>();
        }
        else
        {
            return super.getRenderer(name);
        }
    }

    @Override
    public CellEditor getEditor(String name, IColumn column)
    {
        CellEditor editor = super.getEditor(name, column);

        if (editor == null)
        {
            if (name.toLowerCase().equals(Editor.CONTRACT_TYPE))
            {
                editor = new FieldComboBoxEditor(new ContractTypeComboBox());
            }
        }

        return editor;
    }
}
