package fr.hd3d.humanresources.ui.client.view.widget;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.dialog.FormDialog;
import fr.hd3d.humanresources.ui.client.constant.HumanResourcesConstants;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;


/**
 * Dialog displaying form used to create a new person in database.
 * 
 * @author HD3D
 */
public class PersonDialog extends FormDialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static HumanResourcesConstants CONSTANTS = GWT.create(HumanResourcesConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Login text field. */
    private final TextField<String> loginField = new TextField<String>();
    /** First name text field. */
    private final TextField<String> firstNameField = new TextField<String>();
    /** Last name text field. */
    private final TextField<String> lastNameField = new TextField<String>();

    /**
     * Default constructor.
     */
    public PersonDialog()
    {
        super(null, CONSTANTS.AddNewPerson());
        this.setButtons(Dialog.OKCANCEL);
        this.setStyles();
        this.setForm();
        this.setKeyListeners();
        this.setHtmlIds();
    }

    /**
     * Clear all fields and disable ok button when the dialog is shown.
     * 
     * @see com.extjs.gxt.ui.client.widget.Window#show()
     */
    @Override
    public void show()
    {
        this.firstNameField.clear();
        this.lastNameField.clear();
        this.loginField.clear();
        this.getButtonById(Dialog.OK).setEnabled(false);

        super.show();
    }

    /**
     * Set listeners : hide dialog when a button is clicked, enable ok button when fields are not empty.
     */
    protected void setKeyListeners()
    {
        KeyListener keyListener = new KeyListener() {
            @Override
            public void componentKeyUp(ComponentEvent event)
            {
                updateOkButtonStatus();
                if (event.getKeyCode() == KeyCodes.KEY_ENTER && getButtonById(Dialog.OK).isEnabled())
                {
                    onOkClicked();
                    hide();
                }
            }
        };

        this.loginField.addKeyListener(keyListener);
        this.lastNameField.addKeyListener(keyListener);
        this.firstNameField.addKeyListener(keyListener);
    }

    /**
     * Enable ok button when fields are empty.
     */
    private void updateOkButtonStatus()
    {
        boolean enabled = !Util.isEmptyString(loginField.getValue()) && !Util.isEmptyString(firstNameField.getValue())
                && !Util.isEmptyString(lastNameField.getValue());

        this.getButtonById(Dialog.OK).setEnabled(enabled);
    }

    /**
     * Set person form : login, first name and last name as fields.
     */
    private void setForm()
    {
        loginField.setFieldLabel(COMMON_CONSTANTS.Login());
        this.panel.add(loginField);
        firstNameField.setFieldLabel(COMMON_CONSTANTS.FirstName());
        this.panel.add(firstNameField);
        lastNameField.setFieldLabel(COMMON_CONSTANTS.LastName());
        this.panel.add(lastNameField);
    }

    /**
     * Set dialog styles : size, title...
     */
    private void setStyles()
    {
        this.setWidth(325);
    }

    /**
     * When ok button is clicked a new person object is created in database.
     */
    @Override
    protected void onOkClicked()
    {
        String login = loginField.getValue();
        String firstName = firstNameField.getValue();
        String lastName = lastNameField.getValue();
        if (login != null && login.length() != 0 && firstName != null && firstName.length() != 0 && lastName != null
                && lastName.length() != 0)
        {
            AppEvent event = new AppEvent(HumanResourcesEvents.CREATE_A_PERSON);
            event.setData("firstName", firstName);
            event.setData("lastName", lastName);
            event.setData("login", login);
            EventDispatcher.forwardEvent(event);
            this.hide();
        }
    }

    /**
     * Set HTML IDs to facilitate Selenium testings.
     */
    private void setHtmlIds()
    {
        this.firstNameField.setId("first-name-field");
        this.lastNameField.setId("last-name-field");
        this.loginField.setId("login-field");

        this.getButtonBar().getItem(0).setId("add-person-ok-button");
    }
}
