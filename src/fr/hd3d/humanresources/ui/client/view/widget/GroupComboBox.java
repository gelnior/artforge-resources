package fr.hd3d.humanresources.ui.client.view.widget;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.ResourceGroupReader;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;


/**
 * Group combo box load the full available group list.
 * 
 * @author HD3D
 */
public class GroupComboBox extends ModelDataComboBox<ResourceGroupModelData>
{

    /**
     * Default constructor (set listeners and data reader).
     */
    public GroupComboBox()
    {
        super(new ResourceGroupReader());

        this.setListeners();
    }

    /**
     * Set listeners on data loading and selection changed.
     */
    protected void setListeners()
    {
        this.addSelectionChangedListener(new SelectionChangedListener<ResourceGroupModelData>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<ResourceGroupModelData> se)
            {
                onSelectionChanged(se);
            }

        });
        this.getStore().getLoader().addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                onLoaderLoad(le);
            }
        });
    }

    /**
     * When data are loaded, last selected group is automatically selected (information retrieved from favorite cookie).
     * If no group was selected before, the first group from the store is selected.
     * 
     * @param le
     *            Load event.
     */
    protected void onLoaderLoad(LoadEvent le)
    {
        String groupId = FavoriteCookie.getFavParameterValue(CommonConfig.COOKIE_VAR_GROUP);

        if (groupId != null)
        {
            Long id = Long.parseLong(groupId);
            if (getValue() == null || getValue().getId() != id)
            {
                setValue(getValueById(id));
            }

        }
        else if (getStore().getCount() > 0)
        {
            setValue(getStore().getAt(0));
        }
    }

    /**
     * When selection changed favorite cookie is updated to store last selected group.
     * 
     * @param se
     *            Selection changed event
     */
    protected void onSelectionChanged(SelectionChangedEvent<ResourceGroupModelData> se)
    {
        AppEvent event = new AppEvent(HumanResourcesEvents.ABSENCE_GROUP_CHANGED);
        EventDispatcher.forwardEvent(event);

        ResourceGroupModelData group = se.getSelectedItem();
        if (group != null)
        {
            Long id = group.getId();
            if (id != null)
            {
                FavoriteCookie.putFavValue(CommonConfig.COOKIE_VAR_GROUP, id.toString());
            }
        }
    }
}
