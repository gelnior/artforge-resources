package fr.hd3d.humanresources.ui.client.view.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.listener.EventBaseListener;
import fr.hd3d.common.ui.client.modeldata.reader.EventReader;
import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.EventModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerPanel;
import fr.hd3d.humanresources.ui.client.constant.HumanResourcesConstants;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;


/**
 * Dialog containing a simple event editor (CRUD operations).
 * 
 * @author HD3D
 */
public class EventEditor extends Dialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static HumanResourcesConstants CONSTANTS = GWT.create(HumanResourcesConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Dialog width. */
    public static final int WIDTH = 468;

    /** Simple explorer needed for CRUD operations. */
    SimpleExplorerPanel<EventModelData> eventExplorer = new SimpleExplorerPanel<EventModelData>(new EventReader());

    /**
     * Default constructor : set styles and listeners. It masks the controller by default.
     */
    public EventEditor()
    {
        this.setButtons("");
        this.setStyles();
        this.setEventExplorer();
        this.setListeners();

        this.getController().mask();
    }

    /**
     * @return Simple data explorer controller.
     */
    public MaskableController getController()
    {
        return this.eventExplorer.getController();
    }

    /**
     * When dialog is shown, controller is activated and data are refreshed.
     * 
     * @see com.extjs.gxt.ui.client.widget.Window#show()
     */
    @Override
    public void show()
    {
        this.eventExplorer.refresh();
        this.getController().unMask();
        super.show();
    }

    /**
     * Initialize permissions inside simple data explorer.
     */
    public void initPermissions()
    {
        this.eventExplorer.initPermissions();
    }

    /**
     * Set simple data explorer columns.
     */
    private void setEventExplorer()
    {
        List<ColumnConfig> columnConfigs = new ArrayList<ColumnConfig>();

        ColumnConfig ccTitle = new ColumnConfig();
        ccTitle.setHeader(COMMON_CONSTANTS.Title());
        ccTitle.setId(EventModelData.TITLE_FIELD);
        ccTitle.setDataIndex(EventModelData.TITLE_FIELD);
        ccTitle.setWidth(150);
        ccTitle.setEditor(new CellEditor(new TextField<String>()));
        ccTitle.setResizable(false);
        columnConfigs.add(ccTitle);

        ColumnConfig ccStartDate = new ColumnConfig();
        ccStartDate.setHeader(COMMON_CONSTANTS.StartDate());
        ccStartDate.setId(EventModelData.START_DATE_FIELD);
        ccStartDate.setDataIndex(EventModelData.START_DATE_FIELD);
        ccStartDate.setWidth(140);
        ccStartDate.setDateTimeFormat(DateFormat.FRENCH_DATE);
        ccStartDate.setEditor(new CellEditor(new DateField()));
        ccStartDate.setResizable(false);
        columnConfigs.add(ccStartDate);

        ColumnConfig ccEndDate = new ColumnConfig();
        ccEndDate.setHeader(COMMON_CONSTANTS.EndDate());
        ccEndDate.setId(EventModelData.END_DATE_FIELD);
        ccEndDate.setDataIndex(EventModelData.END_DATE_FIELD);
        ccEndDate.setWidth(140);
        ccEndDate.setDateTimeFormat(DateFormat.FRENCH_DATE);
        ccEndDate.setEditor(new CellEditor(new DateField()));
        ccEndDate.setResizable(false);
        columnConfigs.add(ccEndDate);

        ColumnModel cm = new ColumnModel(columnConfigs);
        eventExplorer.reconfigureGrid(cm);
        eventExplorer.refresh();
        this.add(eventExplorer);
    }

    /**
     * Set styles.
     */
    private void setStyles()
    {
        this.setButtons("");
        this.setLayout(new FitLayout());
        this.setWidth(WIDTH);
        this.setModal(true);
        this.setResizable(false);
        this.setHeading(CONSTANTS.EventEditor());

        this.eventExplorer.setSortField(AbsenceModelData.START_DATE_FIELD);
        this.eventExplorer.setSortDir(SortDir.DESC);
    }

    /**
     * Set hide listeners. When hiding the controller is masked and the ABSENCE_REFRESH_CLICKED event is raised.
     */
    private void setListeners()
    {
        this.addListener(Events.Hide, new EventBaseListener(HumanResourcesEvents.ABSENCE_REFRESH_CLICKED));
        this.addListener(Events.Hide, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                getController().mask();
            }
        });
    }

}
