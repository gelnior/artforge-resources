package fr.hd3d.humanresources.ui.client.view.widget;

import fr.hd3d.common.client.enums.EContractType;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ContractTypeMap;
import fr.hd3d.common.ui.client.widget.FieldComboBox;


public class ContractTypeComboBox extends FieldComboBox
{

    public ContractTypeComboBox()
    {
        super();
        this.setMinListWidth(100);
        this.setTemplate(getXTemplate());

        int i = 1;
        for (EContractType aType : EContractType.values())
        {

            this.addField(i, aType);

        }
    }

    /**
     * Add a new field (couple display value - name) to the combo box.
     * 
     * @param name
     *            Displayed value.
     * @param value
     *            Real value.
     * 
     * @return added field.
     */
    public FieldModelData addField(int id, EContractType type)
    {
        ContractTypeMap map = new ContractTypeMap();
        FieldModelData field = map.get(type.toString());
        field.set("color", ContractTypeMap.getColorForStatus(type.toString()));
        this.getStore().add(field);

        return field;
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
        return  [ 
        '<tpl for=".">', 
        '<div style="text-align: center; background-color:{color};" class="x-combo-list-item"> <span style="font-weight: bold; font-size: 16px;">&nbsp;{name}&nbsp;&nbsp;</span> </div>', 
        '</tpl>' 
        ].join("");
    }-*/;

}
