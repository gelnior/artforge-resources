package fr.hd3d.humanresources.ui.client.view.widget;

import java.util.Date;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.json.client.JSONObject;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ContractTypeMap;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.widget.FieldComboBox;
import fr.hd3d.common.ui.client.widget.PersonComboBox;
import fr.hd3d.common.ui.client.widget.dialog.FormDialog;
import fr.hd3d.humanresources.ui.client.constant.HumanResourcesConstants;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;


/**
 * Dialog displaying form used to create a new person in database.
 * 
 * @author HD3D
 */
public class ContractAmendmentDialog extends FormDialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static HumanResourcesConstants CONSTANTS = GWT.create(HumanResourcesConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Login text field. */
    private final TextField<String> jobField = new TextField<String>();
    /** Salary text field. */
    private final TextField<String> salaryField = new TextField<String>();
    /** TimeField. */
    private final DateField startDateField = new DateField();
    /** TimeField */
    private final DateField endDateField = new DateField();

    private final FieldComboBox type = new ContractTypeComboBox();

    protected final PersonComboBox personComboBox = new PersonComboBox();

    private Hd3dModelData contract = null;
    private PersonModelData currentPerson = null;

    private ContractTypeMap map = new ContractTypeMap();

    /**
     * Default constructor.
     */
    public ContractAmendmentDialog()
    {
        super(null, CONSTANTS.AddNewContractAmendment());
        this.setButtons(Dialog.OKCANCEL);
        this.setStyles();
        this.setForm();
        this.setKeyListeners();
        this.setHtmlIds();
    }

    public Hd3dModelData getContract()
    {
        return contract;
    }

    public void setContract(Hd3dModelData contract)
    {
        this.contract = contract;
    }

    /**
     * Clear all fields and disable ok button when the dialog is shown.
     * 
     * @see com.extjs.gxt.ui.client.widget.Window#show()
     */
    @Override
    public void show()
    {

        JSONObject person = (JSONObject) contract.get("person");
        String obj = (person.get("name").toString()).replace("\"", "");
        this.currentPerson = new PersonModelData();
        this.currentPerson.setId(new Long((person.get("id").toString()).replace("\"", "")));

        this.personComboBox.setForceSelection(true);
        this.personComboBox.setRawValue(obj);
        this.personComboBox.setEnabled(false);

        String job = "Avenant " + contract.get("name").toString();
        this.jobField.setRawValue(job);

        String type = map.get(contract.get("type").toString()).getName();

        this.type.setRawValue(type);

        Date startDate = contract.get("startDate");
        this.startDateField.setValue(startDate);

        Date endDate = contract.get("endDate");
        this.endDateField.setValue(endDate);
        
        Double salary = 0.;
        if(contract.get("salary") != null){
            salary = contract.get("salary");
            this.salaryField.setValue(salary.toString());
        }
        
        this.getButtonById(Dialog.OK).setEnabled(true);
        
        super.show();
    }

    /**
     * Set listeners : hide dialog when a button is clicked, enable ok button when fields are not empty.
     */
    protected void setKeyListeners()
    {
        KeyListener keyListener = new KeyListener() {
            @Override
            public void componentKeyUp(ComponentEvent event)
            {
                updateOkButtonStatus();
                if (event.getKeyCode() == KeyCodes.KEY_ENTER && getButtonById(Dialog.OK).isEnabled())
                {
                    onOkClicked();
                    hide();
                }
            }
        };

        this.personComboBox.setSelectionChangedEvent(HumanResourcesEvents.CONTRACT_PERSON_CHANGED);
        this.personComboBox.addKeyListener(keyListener);
        this.jobField.addKeyListener(keyListener);
    }

    /**
     * Enable ok button when fields are empty.
     */
    public void updateOkButtonStatus()
    {
        
            boolean enabled = this.jobField.getValue() != null;

            this.getButtonById(Dialog.OK).setEnabled(enabled);
        
    }

    /**
     * Set person form : login, first name and last name as fields.
     */
    private void setForm()
    {
        this.type.setEmptyText("Select a type...");

        personComboBox.setFieldLabel("Person");
        this.panel.add(personComboBox);
        jobField.setFieldLabel("Job Title");
        this.panel.add(jobField);
        this.type.setFieldLabel("Type");
        this.panel.add(this.type);
        this.salaryField.setFieldLabel("Daily Salary");
        this.panel.add(this.salaryField);
        startDateField.setFieldLabel("StartDate");
        this.panel.add(startDateField);
        endDateField.setFieldLabel("EndDate");
        this.panel.add(endDateField);

    }

    /**
     * Set dialog styles : size, title...
     */
    private void setStyles()
    {
        this.setWidth(325);
    }

    /**
     * When ok button is clicked a new person object is created in database.
     */
    @Override
    protected void onOkClicked()
    {
        PersonModelData person = personComboBox.getValue();
        String job = "";
        if (this.jobField.getValue() != null)
        {
            job = jobField.getValue();
        }   
        String type = contract.get("type").toString();
        if (this.type.getValue() != null)
        {
            type = ((FieldModelData)(this.type.getValue())).get("value");
        }   
        
        Float salary = null;
        if (this.salaryField.getValue() != null)
        {
             salary = Float.parseFloat(salaryField.getValue());
        }   
        
        Date startDate = startDateField.getValue();
        Date endDate = endDateField.getValue();
     
        AppEvent event = new AppEvent(HumanResourcesEvents.CREATE_CONTRACT_REQUESTED);
        if (currentPerson != null)
        {
            event.setData("person", currentPerson);
        }
        else
        {
            if (person != null)
            {
                event.setData("person", person);
            }
            else
            {
                return;
            }
        }
        event.setData("job", job);
        event.setData("type", type);
        event.setData("startDate", startDate);
        event.setData("endDate", endDate);
        event.setData("salary", salary);
        EventDispatcher.forwardEvent(event);
        this.hide();

    }

    /**
     * Set HTML IDs to facilitate Selenium testings.
     */
    private void setHtmlIds()
    {
        this.jobField.setId("job-field");
        this.startDateField.setId("first-name-field");
        this.endDateField.setId("last-name-field");
        this.personComboBox.setId("person-field");
        this.salaryField.setId("salary-field");

        this.getButtonBar().getItem(0).setId("add-contract-ok-button");
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
        return  [ 
        '<tpl for=".">', 
        '<div style="text-align: center; background-color:{color};" class="x-combo-list-item"> <span style="font-weight: bold; font-size: 16px;">&nbsp;{name}&nbsp;&nbsp;</span> </div>', 
        '</tpl>' 
        ].join("");
    }-*/;
}
