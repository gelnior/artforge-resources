package fr.hd3d.humanresources.ui.client.view.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.listener.EventBaseListener;
import fr.hd3d.common.ui.client.modeldata.reader.AbsenceReader;
import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerPanel;
import fr.hd3d.humanresources.ui.client.constant.HumanResourcesConstants;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.model.modeldata.AbsenceRowModelData;


/**
 * Dialog containing a simple absence editor (CRUD operations).
 * 
 * @author HD3D
 */
public class AbsenceEditor extends Dialog
{
    private static final int WIDTH = 238;

    /** Constant strings to display : dialog messages, button label... */
    public static HumanResourcesConstants CONSTANTS = GWT.create(HumanResourcesConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Simple explorer needed for CRUD operations. */
    private final SimpleExplorerPanel<AbsenceModelData> absenceExplorer = new SimpleExplorerPanel<AbsenceModelData>(
            new AbsenceReader());

    private AbsenceRowModelData selectedRow = null;

    /**
     * Default constructor.
     */
    public AbsenceEditor()
    {
        this.setStyles();
        this.setAbsenceExplorer();
        this.setListeners();
        this.getController().mask();
    }

    /**
     * @return Simple panel controller.
     */
    public MaskableController getController()
    {
        return absenceExplorer.getController();
    }

    /**
     * @return Absences actually in store.
     */
    public List<AbsenceModelData> getAbsences()
    {
        return this.absenceExplorer.getDataAsList();
    }

    /**
     * @return Simple explorer selection.
     */
    public AbsenceRowModelData getSelection()
    {
        return this.selectedRow;
    }

    /**
     * permissions for the data explorer.
     */
    public void initPermissions()
    {
        this.absenceExplorer.initPermissions();
    }

    /**
     * Set dialog styles.
     */
    private void setStyles()
    {
        this.setButtons("");
        this.setLayout(new FitLayout());
        this.setModal(true);
        this.setResizable(false);
        this.setWidth(WIDTH);
        this.setHeading(CONSTANTS.AbsenceEditor());

        this.absenceExplorer.setSortField(AbsenceModelData.START_DATE_FIELD);
        this.absenceExplorer.setSortDir(SortDir.DESC);
    }

    /**
     * Set absence explorer inside the dialog.
     */
    private void setAbsenceExplorer()
    {
        List<ColumnConfig> columnConfigs = new ArrayList<ColumnConfig>();

        ColumnConfig ccStartDate = new ColumnConfig();
        ccStartDate.setHeader(COMMON_CONSTANTS.StartDate());
        ccStartDate.setId(AbsenceModelData.START_DATE_FIELD);
        ccStartDate.setDataIndex(AbsenceModelData.START_DATE_FIELD);
        ccStartDate.setWidth(100);
        ccStartDate.setDateTimeFormat(DateFormat.FRENCH_DATE);
        ccStartDate.setEditor(new CellEditor(new DateField()));
        ccStartDate.setResizable(false);
        columnConfigs.add(ccStartDate);

        ColumnConfig ccEndDate = new ColumnConfig();
        ccEndDate.setHeader(COMMON_CONSTANTS.EndDate());
        ccEndDate.setId(AbsenceModelData.END_DATE_FIELD);
        ccEndDate.setDataIndex(AbsenceModelData.END_DATE_FIELD);
        ccEndDate.setWidth(100);
        ccEndDate.setDateTimeFormat(DateFormat.FRENCH_DATE);
        ccEndDate.setEditor(new CellEditor(new DateField()));
        ccEndDate.setResizable(false);
        columnConfigs.add(ccEndDate);

        ColumnModel cm = new ColumnModel(columnConfigs);
        absenceExplorer.reconfigureGrid(cm);
        absenceExplorer.refresh();

        this.add(absenceExplorer);
    }

    /**
     * Set Hide listeners : mask controller and raise ABSENCE_ABSENCE_EDITOR_HIDE event.
     */
    private void setListeners()
    {
        this.addListener(Events.Hide, new EventBaseListener(HumanResourcesEvents.ABSENCE_ABSENCE_EDITOR_HIDE));
        this.addListener(Events.Hide, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                getController().mask();
            }
        });
    }

    /**
     * Show absence editor for the person with ID equals to <i>id</i>.
     * 
     * @param absenceRow
     *            Person id whom the absences should be displayed.
     */
    public void show(AbsenceRowModelData absenceRow)
    {
        String path = ServicesPath.getPath(PersonModelData.SIMPLE_CLASS_NAME);
        path += absenceRow.getPerson().getId() + "/" + ServicesPath.getPath(AbsenceModelData.SIMPLE_CLASS_NAME);

        this.selectedRow = absenceRow;

        this.absenceExplorer.setPath(path);
        AbsenceModelData defaultAbsence = new AbsenceReader().newModelInstance();
        defaultAbsence.setWorkerID(absenceRow.getPerson().getId());
        this.absenceExplorer.setDefaultInstance(defaultAbsence);
        this.absenceExplorer.refresh();

        this.getController().unMask();
        super.show();
    }
}
