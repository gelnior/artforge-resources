package fr.hd3d.humanresources.ui.client.view.icon;

import com.extjs.gxt.ui.client.data.ModelIconProvider;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.humanresources.ui.client.images.ResourceImages;


/**
 * Icon provider for group tree. It returns appropriate icon depending on node type.
 * 
 * @author HD3D
 */
public class GroupTreeIconProvider implements ModelIconProvider<RecordModelData>
{
    /** Store to access tree data. */
    private final TreeStore<RecordModelData> store;

    /**
     * @param store
     */
    public GroupTreeIconProvider(TreeStore<RecordModelData> store)
    {
        this.store = store;
    }

    /**
     * Returns appropriate icon depending on <i>model</i> class name. If <i>model</i> is a person, it checks if it is
     * the group leader.
     * 
     * @param model
     *            The node/leaf on which the icon will be set.
     * 
     * @see com.extjs.gxt.ui.client.data.ModelIconProvider#getIcon(com.extjs.gxt.ui.client.data.ModelData)
     */
    public AbstractImagePrototype getIcon(RecordModelData model)
    {
        if (ResourceGroupModelData.CLASS_NAME.equals(model.getClassName()))
        {
            return ResourceImages.getGroupIcon();
        }
        else
        {
            RecordModelData parent = store.getParent(model);

            Long leaderId = null;
            if (parent != null)
            {
                leaderId = parent.get(ResourceGroupModelData.LEADER_ID_FIELD);
            }
            if (leaderId != null && leaderId == model.getId().longValue())
            {
                return ResourceImages.getLeaderIcon();
            }
            else
            {
                return ResourceImages.getPersonIcon();
            }
        }
    }
}
