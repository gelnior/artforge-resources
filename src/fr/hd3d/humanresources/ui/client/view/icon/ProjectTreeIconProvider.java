package fr.hd3d.humanresources.ui.client.view.icon;

import com.extjs.gxt.ui.client.data.ModelIconProvider;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.humanresources.ui.client.images.ResourceImages;


/**
 * Icon provider for project tree. It returns appropriate icon depending on node type.
 * 
 * @author HD3D
 */
public class ProjectTreeIconProvider implements ModelIconProvider<RecordModelData>
{
    /**
     * Returns appropriate icon depending on <i>model</i> class name.
     * 
     * @param model
     *            The node/leaf on which the icon will be set.
     * 
     * @see com.extjs.gxt.ui.client.data.ModelIconProvider#getIcon(com.extjs.gxt.ui.client.data.ModelData)
     */
    public AbstractImagePrototype getIcon(RecordModelData model)
    {
        if (ResourceGroupModelData.CLASS_NAME.equals(model.getClassName()))
        {
            return ResourceImages.getGroupIcon();
        }
        else
        {
            return Hd3dImages.getProjectIcon();
        }
    }
}
