package fr.hd3d.humanresources.ui.client.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.dnd.DND.Feedback;
import com.extjs.gxt.ui.client.dnd.DND.Operation;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.KeyUpListener;
import fr.hd3d.common.ui.client.modeldata.NameModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.basetree.BaseTree;
import fr.hd3d.common.ui.client.widget.dialog.NameDialog;
import fr.hd3d.humanresources.ui.client.constant.HumanResourcesConstants;
import fr.hd3d.humanresources.ui.client.controller.GroupController;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.model.GroupModel;
import fr.hd3d.humanresources.ui.client.model.loader.GroupTreeDataLoader;
import fr.hd3d.humanresources.ui.client.model.loader.ProjectTreeDataLoader;
import fr.hd3d.humanresources.ui.client.view.dnd.GroupDragSource;
import fr.hd3d.humanresources.ui.client.view.dnd.GroupTreePanelDropTarget;
import fr.hd3d.humanresources.ui.client.view.dnd.PersonGridDragSource;
import fr.hd3d.humanresources.ui.client.view.dnd.ProjectTreePanelDropTarget;
import fr.hd3d.humanresources.ui.client.view.dnd.RecordTreePanelDropTarget;
import fr.hd3d.humanresources.ui.client.view.icon.GroupTreeIconProvider;
import fr.hd3d.humanresources.ui.client.view.icon.ProjectTreeIconProvider;
import fr.hd3d.humanresources.ui.client.view.menu.GroupTreeMenu;
import fr.hd3d.humanresources.ui.client.view.menu.ProjectTreeMenu;
import fr.hd3d.humanresources.ui.client.view.tree.GroupTree;
import fr.hd3d.humanresources.ui.client.view.tree.ProjectTree;


/**
 * This view provides all stuff to manage person groups.
 * 
 * @author HD3D
 */
public class GroupView extends BorderedPanel implements IGroupView
{
    /** Constant strings to display : dialog messages, button label... */
    public static HumanResourcesConstants CONSTANTS = GWT.create(HumanResourcesConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Model containing data stores. */
    private final GroupModel groupModel = new GroupModel();
    /** Controller that handles group events. */
    private final GroupController controller = new GroupController(groupModel, this);

    /** Tree displaying groups and persons. */
    private final GroupTree groupTree = new GroupTree();
    /** Tree displaying groups attached to projects. */
    private final BaseTree<ResourceGroupModelData, ProjectModelData> projectTree = new ProjectTree();

    /** The grid used to select persons. */
    private Grid<PersonModelData> personGrid;
    /** Text field needed for filtering person selection. */
    private final TextField<String> personFilterTextField = new TextField<String>();
    /** The tool bar containing the person filter text field. */
    private final ToolBar personToolBar = new ToolBar();

    /** Dialog used to create a new group. */
    private final NameDialog createGroupDialog = new NameDialog(HumanResourcesEvents.GROUP_CREATE_GROUP_CONFIRMED,
            CONSTANTS.CreateGroup());
    /** Dialog used to rename a group. */
    private final NameDialog renameGroupDialog = new NameDialog(HumanResourcesEvents.GROUP_RENAME_GROUP_CONFIRMED,
            CONSTANTS.RenameGroup());

    /**
     * Default constructor : set all widgets : filter : absence grid and absence/event forms.
     */
    public GroupView()
    {
        super();

        this.initPersonList();
        this.initGroupTree();
        this.initProjectTree();
        this.initTreeData();
        this.setShadow(false);

        EventDispatcher.get().addController(controller);
        this.controller.handleEvent(new AppEvent(CommonEvents.PERMISSION_INITIALIZED));
    }

    /**
     * @return Selected items from group tree.
     */
    public List<RecordModelData> getGroupSelection()
    {
        return this.groupTree.getSelection();
    }

    /**
     * Expand a group node from the group tree.
     */
    public void expand(RecordModelData group)
    {
        this.groupTree.expand(group);
    }

    /**
     * Display the create group dialog.
     */
    public void displayCreateGroupDialog()
    {
        createGroupDialog.show();
        createGroupDialog.clearNameField();
    }

    /**
     * Display the rename group dialog.
     */
    public void showRenameDialog()
    {
        renameGroupDialog.clearNameField();
        renameGroupDialog.show();
    }

    /**
     * Refresh node corresponding to group.
     */
    public void refreshGroup(RecordModelData group)
    {
        groupTree.getTree().setExpanded(group, false);
        groupTree.getTree().setExpanded(group, true);
    }

    /**
     * Enable or disable group tree.
     * 
     * @param enabled
     *            True to enable group tree, false either.
     */
    public void setGroupTreeEnabled(boolean enabled)
    {
        this.groupTree.setEnabled(enabled);
    }

    /**
     * Expand a project node from project tree.
     */
    public void expandProject(RecordModelData recordModelData)
    {
        this.projectTree.expand(recordModelData);
    }

    /**
     * Return selected items from project tree.
     */
    public List<RecordModelData> getProjectSelection()
    {
        return this.projectTree.getSelection();
    }

    /**
     * Return the filter value given by the user.
     */
    public String getFilterValue()
    {
        return this.personFilterTextField.getValue();
    }

    /**
     * Load data for group tree and project tree. Register the tree stores to the model.
     */
    private void initTreeData()
    {
        this.groupModel.setGroupStore(this.groupTree.getStore());
        this.groupModel.setGroupLoader((GroupTreeDataLoader) this.groupTree.getLoader());
        this.groupModel.setProjectStore(this.projectTree.getStore());
        this.groupModel.setProjectLoader((ProjectTreeDataLoader) this.projectTree.getLoader());

        this.projectTree.setCurrentProject(null);
        this.groupTree.setCurrentProject(null);
    }

    /**
     * Build person grid on the left of the group view. Person grid is used for selecting persons to put in groups.
     */
    private void initPersonList()
    {
        List<ColumnConfig> columnConfigs = new ArrayList<ColumnConfig>();
        ColumnConfig nameCC = new ColumnConfig();
        nameCC.setId(NameModelData.NAME_FIELD);
        nameCC.setDataIndex(NameModelData.NAME_FIELD);
        columnConfigs.add(nameCC);

        ColumnModel cm = new ColumnModel(columnConfigs);
        this.personGrid = new Grid<PersonModelData>(this.groupModel.getPersonStore(), cm);
        this.personGrid.setAutoExpandColumn(NameModelData.NAME_FIELD);
        this.personGrid.setHideHeaders(true);

        this.personFilterTextField.setEmptyText(CONSTANTS.TypeWorkerLastName());
        this.personFilterTextField.setToolTip(CONSTANTS.TypeWorkerLastName());
        this.personFilterTextField.setWidth(185);
        this.personFilterTextField.addListener(Events.KeyUp, new KeyUpListener(
                HumanResourcesEvents.GROUP_PERSON_FILTER_CHANGED));

        this.personToolBar.setStyleAttribute("padding", "5px");
        this.personToolBar.add(personFilterTextField);

        ContentPanel personPanel = new ContentPanel();
        personPanel.setBorders(false);
        personPanel.setHeaderVisible(true);
        personPanel.setHeading(CONSTANTS.WorkerSearch());
        personPanel.setLayout(new FitLayout());
        personPanel.add(this.personGrid);
        personPanel.setTopComponent(this.personToolBar);

        this.addWest(personPanel);
    }

    /**
     * Initialize group tree and set it into the center.
     */
    private void initGroupTree()
    {
        groupTree.setBorders(false);
        groupTree.setStateful(true);
        groupTree.getTree().setDisplayProperty(RecordModelData.NAME_FIELD);
        groupTree.getTree().setIconProvider(new GroupTreeIconProvider(groupTree.getStore()));

        this.addCenter(groupTree);

        this.initContextMenus();
        this.initDragNDrop();
    }

    /**
     * Initialize the project tree and set it to east.
     */
    private void initProjectTree()
    {
        projectTree.getTree().setIconProvider(new ProjectTreeIconProvider());
        this.addEast(projectTree);
    }

    /**
     * Create project tree context menu and group tree context menu.
     */
    private void initContextMenus()
    {
        final GroupTreeMenu groupContextMenu = new GroupTreeMenu(groupTree);
        groupTree.setContextMenu(groupContextMenu);

        final ProjectTreeMenu projectContextMenu = new ProjectTreeMenu(projectTree.getTree());
        projectTree.setContextMenu(projectContextMenu);
    }

    /**
     * Set drag'n'drop on grids and person list.
     */
    private void initDragNDrop()
    {
        new PersonGridDragSource(personGrid, groupTree.getTree());

        new GroupDragSource(groupTree.getTree());

        RecordTreePanelDropTarget target = new GroupTreePanelDropTarget(groupTree.getTree());
        target.setAllowSelfAsSource(true);
        target.setFeedback(Feedback.APPEND);
        target.setOperation(Operation.MOVE);
        target.setAutoExpand(false);
        target.setAllowDropOnLeaf(true);

        RecordTreePanelDropTarget projectTarget = new ProjectTreePanelDropTarget(projectTree.getTree());
        projectTarget.setAllowSelfAsSource(false);
        projectTarget.setFeedback(Feedback.APPEND);
        projectTarget.setOperation(Operation.COPY);
        projectTarget.setAllowDropOnLeaf(true);
    }
}
