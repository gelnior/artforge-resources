package fr.hd3d.humanresources.ui.client.view.dnd;

import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


public class GroupTreePanelDropTarget extends RecordTreePanelDropTarget
{

    public GroupTreePanelDropTarget(TreePanel<RecordModelData> tree)
    {
        super(tree);
    }

    @Override
    protected void showFeedback(DNDEvent event)
    {
        super.showFeedback(event);
        RecordModelData target = ((RecordTreePanelDropTarget) event.getDropTarget()).getActiveRecord();
        if (target == null)
        {
            event.getStatus().setStatus(false);
            return;
        }
        if (ResourceGroupModelData.CLASS_NAME.equals(target.getClassName()))
        {
            event.getStatus().setStatus(true);
        }
        else
        {
            event.getStatus().setStatus(false);
        }
    }

}
