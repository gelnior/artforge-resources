package fr.hd3d.humanresources.ui.client.view.dnd;

import java.util.List;

import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.store.TreeStoreModel;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


/**
 * ProjectTreePanelDropTarget doesn't allow to drop person on project tree.
 * 
 * @author HD3D
 */
public class ProjectTreePanelDropTarget extends RecordTreePanelDropTarget
{

    public ProjectTreePanelDropTarget(TreePanel<RecordModelData> tree)
    {
        super(tree);
    }

    /**
     * Check if source is a person. If yes, it doesn't allow to drop person on project tree.
     */
    @Override
    protected void showFeedback(DNDEvent event)
    {
        super.showFeedback(event);
        if (!event.getStatus().getStatus())
        {
            return;
        }
        List<?> dataSource = (List<?>) event.getDragSource().getData();
        Object obj = dataSource.get(0);
        if (obj instanceof TreeStoreModel)
        {
            TreeStoreModel sourceModel = (TreeStoreModel) obj;
            RecordModelData source = (RecordModelData) sourceModel.getModel();
            RecordModelData target = ((RecordTreePanelDropTarget) event.getDropTarget()).getActiveRecord();
            if (PersonModelData.CLASS_NAME.equals(source.getClassName()) || source.getId() <= 0)
            {
                event.getStatus().setStatus(false);
            }
            else if (ResourceGroupModelData.CLASS_NAME.equals(target.getClassName()))
            {
                event.getStatus().setStatus(false);
            }

        }
        else
        {
            event.getStatus().setStatus(false);
        }
    }
}
