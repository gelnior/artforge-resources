package fr.hd3d.humanresources.ui.client.view.dnd;

import java.util.List;

import com.extjs.gxt.ui.client.dnd.GridDragSource;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;


/**
 * Drag source for person grid.
 * 
 * @author HD3D
 */
public class PersonGridDragSource extends GridDragSource
{
    private final TreePanel<RecordModelData> tree;

    public PersonGridDragSource(Grid<PersonModelData> grid, TreePanel<RecordModelData> tree)
    {
        super(grid);
        this.tree = tree;
    }

    /**
     * When the item is dropped, the data source, the parent target and the target are stored in GROUP_PERSON_DROPPED
     * event which is raised after.
     * 
     * @param event
     *            The drop event.
     * 
     * @see com.extjs.gxt.ui.client.dnd.TreePanelDragSource#onDragDrop(com.extjs.gxt.ui.client.event.DNDEvent)
     */
    @Override
    protected void onDragDrop(DNDEvent event)
    {
        RecordModelData target = ((RecordTreePanelDropTarget) event.getDropTarget()).getActiveRecord();
        List<?> dataSource = (List<?>) event.getDragSource().getData();
        RecordModelData parent = tree.getStore().getParent(target);

        AppEvent mvcEvent = new AppEvent(HumanResourcesEvents.GROUP_PERSON_DROPPED);

        mvcEvent.setData(HumanResourcesEvents.EVENT_VAR_PARENT, parent);
        mvcEvent.setData(HumanResourcesEvents.EVENT_VAR_SOURCE, dataSource);
        mvcEvent.setData(HumanResourcesEvents.EVENT_VAR_TARGET, target);

        EventDispatcher.forwardEvent(mvcEvent);
    }
}
