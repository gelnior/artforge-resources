package fr.hd3d.humanresources.ui.client.view.dnd;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.dnd.TreePanelDragSource;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.TreeStoreModel;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;


/**
 * Drag source for group tree.
 * 
 * @author HD3D
 */
public class GroupDragSource extends TreePanelDragSource
{
    /** Parent of the dragged node. */
    private RecordModelData parent;

    /**
     * Default constructor.
     * 
     * @param tree
     *            The source tree.
     */
    public GroupDragSource(TreePanel<RecordModelData> tree)
    {
        super(tree);
    }

    /**
     * When drag starts the parent of the item dragged is stored.
     * 
     * @param event
     *            The drag event.
     * 
     * @see com.extjs.gxt.ui.client.dnd.TreePanelDragSource#onDragStart(com.extjs.gxt.ui.client.event.DNDEvent)
     */
    @Override
    public void onDragStart(DNDEvent event)
    {
        RecordModelData sel = (RecordModelData) tree.getSelectionModel().getSelectedItem();

        if (sel == null || sel == tree.getStore().getRootItems().get(0))
        {
            event.setCancelled(true);
            event.getStatus().setStatus(false);
            return;
        }
        String className = sel.getClassName();
        try
        {
            for (int i = 0; i < tree.getSelectionModel().getSelectedItems().size(); i++)
            {
                RecordModelData record = (RecordModelData) tree.getSelectionModel().getSelectedItems().get(i);
                if (!record.getClassName().equals(className))
                {

                    throw new Hd3dException("You only can drag'n drop groups or persons, not both.");

                }
            }
            parent = (RecordModelData) tree.getStore().getParent(sel);
            super.onDragStart(event);
        }
        catch (Hd3dException e)
        {
            e.print();
            event.setCancelled(true);
            event.getStatus().setStatus(false);
            return;
        }
    }

    /**
     * When the item is dropped, the data source, the parent source and the target are stored in GROUP_GROUP_DROPPED
     * event which is raised after.
     * 
     * @param event
     *            The drop event.
     * 
     * @see com.extjs.gxt.ui.client.dnd.TreePanelDragSource#onDragDrop(com.extjs.gxt.ui.client.event.DNDEvent)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void onDragDrop(DNDEvent event)
    {
        RecordModelData target = ((RecordTreePanelDropTarget) event.getDropTarget()).getActiveRecord();
        if (target == null)
            return;

        super.onDragDrop(event);

        List<TreeStoreModel> dataSource = (List<TreeStoreModel>) event.getDragSource().getData();
        ArrayList<RecordModelData> sources = new ArrayList<RecordModelData>();
        for (TreeStoreModel sourceModel : dataSource)
        {
            RecordModelData record = (RecordModelData) sourceModel.getModel();
            sources.add(record);
        }

        AppEvent mvcEvent = new AppEvent(HumanResourcesEvents.GROUP_GROUP_DROPPED);

        mvcEvent.setData(HumanResourcesEvents.EVENT_VAR_PARENT, parent);
        mvcEvent.setData(HumanResourcesEvents.EVENT_VAR_SOURCE, sources);
        mvcEvent.setData(HumanResourcesEvents.EVENT_VAR_TARGET, target);

        EventDispatcher.forwardEvent(mvcEvent);

    }
}
