package fr.hd3d.humanresources.ui.client.view.dnd;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.dnd.TreePanelDropTarget;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel.TreeNode;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


/**
 * RecordTreePanelDropTarget checks if source already exists inside target node before adding it.
 * 
 * @author HD3D
 */
public class RecordTreePanelDropTarget extends TreePanelDropTarget
{
    public RecordTreePanelDropTarget(TreePanel<RecordModelData> tree)
    {
        super(tree);
    }

    public RecordModelData getActiveRecord()
    {
        if (this.activeItem == null)
        {
            return null;
        }
        return (RecordModelData) this.activeItem.getModel();
    }

    /**
     * Checks if source already exists inside target node before adding it.
     * 
     * @see com.extjs.gxt.ui.client.dnd.TreePanelDropTarget#handleAppendDrop(com.extjs.gxt.ui.client.event.DNDEvent,
     *      com.extjs.gxt.ui.client.widget.treepanel.TreePanel.TreeNode)
     */
    @Override
    protected void handleAppendDrop(DNDEvent event, @SuppressWarnings("rawtypes") TreeNode item)
    {
        List<ModelData> sel = prepareDropData(event.getData(), false);
        if (item != null)
        {
            ArrayList<ModelData> modelToDelete = new ArrayList<ModelData>();
            RecordModelData parent = (RecordModelData) item.getModel();

            List<ModelData> children = tree.getStore().getChildren(parent);
            for (ModelData model : sel)
            {
                RecordModelData record = null;
                if (model instanceof PersonModelData)
                {
                    record = (RecordModelData) model;
                }
                else
                {
                    record = model.get("model");
                }
                for (ModelData child : children)
                {
                    if (child.get(Hd3dModelData.ID_FIELD) != null
                            && ((Long) child.get(Hd3dModelData.ID_FIELD)).longValue() == record.getId().longValue())
                    {
                        modelToDelete.add(model);
                        break;
                    }
                }
            }
            sel.removeAll(modelToDelete);
        }
        if (sel.size() > 0)
        {
            ModelData p = null;
            if (item != null)
            {
                p = item.getModel();
                appendModel(p, sel, tree.getStore().getChildCount(item.getModel()));
            }
        }
    }

    @Override
    protected void showFeedback(DNDEvent event)
    {

        super.showFeedback(event);
        RecordModelData target = ((RecordTreePanelDropTarget) event.getDropTarget()).getActiveRecord();
        if (target == null)
        {
            event.getStatus().setStatus(false);
            return;
        }

    }
}
