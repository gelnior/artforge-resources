package fr.hd3d.humanresources.ui.client.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.HeaderGroupConfig;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.i18n.client.constants.DateTimeConstants;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.EventBaseListener;
import fr.hd3d.common.ui.client.modeldata.NameModelData;
import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.humanresources.ui.client.constant.HumanResourcesConstants;
import fr.hd3d.humanresources.ui.client.controller.AbsenceController;
import fr.hd3d.humanresources.ui.client.event.HumanResourcesEvents;
import fr.hd3d.humanresources.ui.client.model.AbsenceModel;
import fr.hd3d.humanresources.ui.client.model.modeldata.AbsenceRowModelData;
import fr.hd3d.humanresources.ui.client.view.renderer.AbsenceRenderer;
import fr.hd3d.humanresources.ui.client.view.widget.AbsenceEditor;
import fr.hd3d.humanresources.ui.client.view.widget.EventEditor;
import fr.hd3d.humanresources.ui.client.view.widget.GroupComboBox;


/**
 * This view provides all stuff to handle worker absences.
 * 
 * @author HD3D
 */
public class AbsenceView extends BorderedPanel implements IAbsenceView
{
    /** Constant strings to display : dialog messages, button label... */
    public static HumanResourcesConstants CONSTANTS = GWT.create(HumanResourcesConstants.class);
    /** Constant strings from common library. */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Model containing person and absence row store. Absence row is a model data used to display absence for one person
     * on one line.
     */
    private final AbsenceModel model = new AbsenceModel();
    /** Controller that handles absence events. */
    private final AbsenceController controller = new AbsenceController(model, this);

    /** Start date field needed by date filter. */
    private final DateField startDateFilter = new DateField();
    /** End date field needed by date filter. */
    private final DateField endDateFilter = new DateField();

    /** When clicked, this button refreshes the absence grid. */
    private final ToolBarButton refreshButton = new ToolBarButton(Hd3dImages.getRefreshIcon(), "Refresh",
            HumanResourcesEvents.ABSENCE_REFRESH_CLICKED);
    /** When clicked, this button shows the event editor dialog. */
    private final ToolBarButton eventButton = new ToolBarButton("event-editor-icon", "Edit events",
            HumanResourcesEvents.ABSENCE_EVENT_EDITOR_CLICKED);
    /** When clicked, this button shows the absence editor dialog. */
    private final ToolBarButton absenceButton = new ToolBarButton("absence-editor-icon", "Edit absences",
            HumanResourcesEvents.ABSENCE_ABSENCE_EDITOR_CLICKED);

    /** Grid used to display absences. */
    private Grid<AbsenceRowModelData> grid;

    /** Simple explorer dialog with columns adapted to event edition. */
    private final EventEditor eventEditor = new EventEditor();
    /** Simple explorer with columns adapted to absence edition. */
    private final AbsenceEditor absenceEditor = new AbsenceEditor();

    /**
     * When you select a group in that combo box, absence grid is refreshed with absence data of persons from that
     * group.
     */
    private final GroupComboBox groupCombo = new GroupComboBox();

    /**
     * Default constructor. It set all widgets : filter : absence grid and absence/event forms. It registers controller.
     */
    public AbsenceView()
    {
        super();
        EventDispatcher.get().addController(controller);

        this.setStyles();
        this.initToolbar();
        this.initFilter();
        this.createGrid();
        this.initEventEditor();
        this.initAbsenceEditor();

        this.controller.handleEvent(new AppEvent(CommonEvents.PERMISSION_INITIALIZED));
        this.groupCombo.getStore().getLoader().load();
    }

    /**
     * Rebuild grid with a new column model depending on the date filters.
     */
    public void rebuildGrid()
    {
        ColumnModel cm = this.getColumnModel(this.getStartDate(), this.getEndDate());
        this.grid.reconfigure(this.model.getAbsenceListStore(), cm);

        this.controller.handleEvent(new AppEvent(HumanResourcesEvents.ABSENCE_GRID_CREATED));
    }

    /**
     * @return Start date field value.
     */
    public Date getStartDate()
    {
        return this.startDateFilter.getValue();
    }

    /**
     * @return Start date field value.
     */
    public Date getEndDate()
    {
        return this.endDateFilter.getValue();
    }

    /**
     * @return Absence grid selected rows.
     */
    public List<AbsenceRowModelData> getSelection()
    {
        return this.grid.getSelectionModel().getSelectedItems();
    }

    /**
     * Show event editor dialog.
     */
    public void showEventEditor()
    {
        this.eventEditor.show();
    }

    /**
     * Show absence editor dialog (only if there is one and only one record selected in the absence grid).
     */
    public void showAbsenceEditor()
    {
        List<AbsenceRowModelData> selection = this.grid.getSelectionModel().getSelection();

        if (selection.size() == 1)
        {
            this.absenceEditor.show(selection.get(0));
        }
    }

    /**
     * @return Selected group in group combo box (group currently displayed).
     */
    public ResourceGroupModelData getCurrentGroup()
    {
        return this.groupCombo.getValue();
    }

    /**
     * Mask grid and display a loading message.
     */
    public void maskGrid()
    {
        this.grid.mask(COMMON_CONSTANTS.Loading());
    }

    /**
     * Unmask grid and remove loading message.
     */
    public void unMaskGrid()
    {
        this.grid.unmask();
    }

    /**
     * Set Group combo enabled if enabled is true, set it disabled either.
     * 
     * @param enabled
     *            True to enable, false to disable.
     */
    public void setGroupComboEnabled(boolean enabled)
    {
        this.groupCombo.setEnabled(enabled);
    }

    /**
     * Set date pickers enabled if enabled is true, set it disabled either.
     * 
     * @param enabled
     *            True to enable, false to disable.
     */
    public void setDatePickerEnabled(boolean enabled)
    {
        this.startDateFilter.setEnabled(enabled);
        this.endDateFilter.setEnabled(enabled);
    }

    /**
     * Set absence editor button enabled if enabled is true, set it disabled either.
     * 
     * @param enabled
     *            True to enable, false to disable.
     */
    public void setAbsenceButtonEnabled(boolean enabled)
    {
        this.absenceButton.setEnabled(enabled);
    }

    /**
     * Set event editor button enabled if enabled is true, set it disabled either.
     * 
     * @param enabled
     *            True to enable, false to disable.
     */
    public void setEventButtonEnabled(boolean enabled)
    {
        this.eventButton.setEnabled(enabled);
    }

    /**
     * Set refresh button enabled if enabled is true, set it disabled either.
     * 
     * @param enabled
     *            True to enable, false to disable.
     */
    public void setRefreshEnabled(boolean enabled)
    {
        this.refreshButton.setEnabled(enabled);
    }

    /**
     * Ask for absence and event editor to hide/show their widgets depending on permissions.
     */
    public void initEditorPermissions()
    {
        this.eventEditor.initPermissions();
        this.absenceEditor.initPermissions();
    }

    /**
     * @return row contained in absence editor (useful to refresh grid after absence edition).
     */
    public List<AbsenceModelData> getAbsencesFromEditor()
    {
        return this.absenceEditor.getAbsences();
    }

    /**
     * @return Absence selected in model data.
     */
    public AbsenceRowModelData getAbsenceEditorSelection()
    {
        return this.absenceEditor.getSelection();
    }

    /**
     * Initialize filter form with first day of the current month for start date and last day of the current month for
     * end date.
     */
    private void initFilter()
    {
        DateWrapper date = new DateWrapper().clearTime();

        DateWrapper beginDate = date.getFirstDayOfMonth();
        DateWrapper endDate = date.getLastDateOfMonth();

        this.startDateFilter.setValue(beginDate.asDate());
        this.endDateFilter.setValue(endDate.asDate());

        this.startDateFilter.addListener(Events.Change, new EventBaseListener(
                HumanResourcesEvents.ABSENCE_FILTER_CHANGED));
        this.endDateFilter.addListener(Events.Change,
                new EventBaseListener(HumanResourcesEvents.ABSENCE_FILTER_CHANGED));
    }

    /**
     * Register absence editor controller as child of absence view.
     */
    private void initAbsenceEditor()
    {
        this.controller.addChild(absenceEditor.getController());
    }

    /**
     * Register event editor controller as child of absence view.
     */
    private void initEventEditor()
    {
        this.controller.addChild(eventEditor.getController());
    }

    /**
     * Initialize absence tool bar with start date filter and end date filter.
     */
    private void initToolbar()
    {
        this.setToolBar();

        this.addToToolBar(groupCombo);
        this.addToToolBar(new SeparatorToolItem());
        this.addToToolBar(new LabelField(CONSTANTS.From()));
        this.addToToolBar(startDateFilter);
        this.addToToolBar(new LabelField(CONSTANTS.To()));
        this.addToToolBar(endDateFilter);
        this.addToToolBar(new SeparatorToolItem());

        this.addToToolBar(refreshButton);
        this.addToToolBar(eventButton);
        this.addToToolBar(absenceButton);

        this.setAbsenceButtonEnabled(false);
        this.setDatePickerEnabled(false);
        this.setRefreshEnabled(false);
    }

    /**
     * Set widget styles.
     */
    private void setStyles()
    {
        startDateFilter.setWidth(80);
        endDateFilter.setWidth(80);
    }

    /**
     * Create absence grid and add it to the center.
     */
    private void createGrid()
    {
        ColumnModel cm = this.getColumnModel(this.getStartDate(), this.getEndDate());
        this.grid = new Grid<AbsenceRowModelData>(this.model.getAbsenceListStore(), cm);
        this.grid.setBorders(true);

        this.addCenter(grid);
    }

    /**
     * @param begin
     *            Begin date of absence displayed.
     * @param end
     *            End date of absence displayed.
     * @return Column model containing one column for the person full name (first + last) and one column for each from
     *         <i>begin</i> date to <i>end</i> date.
     */
    private ColumnModel getColumnModel(Date begin, Date end)
    {
        List<ColumnConfig> columnConfigs = new ArrayList<ColumnConfig>();
        DateWrapper beginDate = new DateWrapper(begin);
        DateWrapper endDate = new DateWrapper(end);

        this.addNameColumn(columnConfigs);
        this.addDayColumns(columnConfigs, beginDate, endDate);

        beginDate = new DateWrapper(begin);
        ColumnModel cm = new ColumnModel(columnConfigs);
        this.addHeaders(cm, beginDate, endDate);

        return cm;
    }

    /**
     * Add a column displaying person full name.
     * 
     * @param columnConfigs
     *            The column config list on which to add the name column.
     */
    private void addNameColumn(List<ColumnConfig> columnConfigs)
    {
        ColumnConfig nameCC = new ColumnConfig();
        nameCC.setDataIndex(NameModelData.NAME_FIELD);
        nameCC.setHeader(CONSTANTS.Person());
        nameCC.setWidth(160);
        columnConfigs.add(nameCC);
    }

    /**
     * Add a day column for each date between <i>beginDate</i> and <i>endDate</i>.
     * 
     * @param columnConfigs
     *            The column config list on which to add the day columns.
     * @param beginDate
     *            Begin date for day column list.
     * @param endDate
     *            End date for day column list.
     */
    private void addDayColumns(List<ColumnConfig> columnConfigs, DateWrapper beginDate, DateWrapper endDate)
    {
        AbsenceRenderer absenceRenderer = new AbsenceRenderer();

        while (beginDate.before(endDate.addDays(1)))
        {
            ColumnConfig cc = new ColumnConfig();

            String key = beginDate.getFullYear() + "-" + beginDate.getMonth() + "-" + beginDate.getDate();
            cc.setDataIndex(key);
            cc.setHeader("" + beginDate.getDate());
            cc.setWidth(25);
            cc.setResizable(false);
            cc.setRenderer(absenceRenderer);
            columnConfigs.add(cc);
            beginDate = beginDate.addDays(1);
        }
    }

    /**
     * Add Headers to the grid column model for each day column.
     * 
     * @param cm
     *            The column model.
     * @param beginDate
     *            The begin date of day columns.
     * @param endDate
     *            The end date of day columns.
     */
    private void addHeaders(ColumnModel cm, DateWrapper beginDate, DateWrapper endDate)
    {
        int column = 1;
        DateWrapper iterator = new DateWrapper(beginDate.asDate());

        while (iterator.before(endDate))
        {
            int width = 0;
            if (iterator.getMonth() != endDate.getMonth())
            {
                width = iterator.getLastDateOfMonth().getDate() - iterator.getDate() + 1;
            }
            else
            {
                width = endDate.getDate() - iterator.getDate() + 1;
            }

            DateTimeConstants constants = LocaleInfo.getCurrentLocale().getDateTimeConstants();
            cm.addHeaderGroup(0, column, new HeaderGroupConfig(constants.months()[iterator.getMonth()], 1, width));

            column += width;
            iterator = iterator.addMonths(1);
            if (iterator.getDate() != 1)
            {
                iterator = iterator.getFirstDayOfMonth();
            }
        }
    }

}
