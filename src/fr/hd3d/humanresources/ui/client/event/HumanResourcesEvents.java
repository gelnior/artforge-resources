package fr.hd3d.humanresources.ui.client.event;

import com.extjs.gxt.ui.client.event.EventType;
import com.google.gwt.user.client.ui.AbstractImagePrototype;


/**
 * BreakdownEvents contains all event code values sent to the event dispatcher.
 * 
 * @author HD3D
 */
public class HumanResourcesEvents
{

    public static final String EVENT_VAR_TARGET = "event";
    public static final String EVENT_VAR_SOURCE = "source";
    public static final String EVENT_VAR_PARENT = "parent";

    public static int START_EVENT = 16000;
    public static final EventType SAVE = new EventType(START_EVENT++);
    public static final EventType REFRESH = new EventType(START_EVENT++);
    public static final EventType DELETE_SUCCESS = new EventType(START_EVENT++);
    public static final EventType CREATE_SUCCESS = new EventType(START_EVENT++);
    public static final EventType UPDATE_SUCCESS = new EventType(START_EVENT++);
    public static final EventType UNDO = new EventType(START_EVENT++);
    public static final EventType REDO = new EventType(START_EVENT++);

    public static final EventType ERROR = new EventType(START_EVENT++);
    public static final EventType INIT = new EventType(START_EVENT++);
    public static final EventType INIT_VIEW = new EventType(START_EVENT++);
    public static final EventType INIT_SUCCESS = new EventType(START_EVENT++);
    public static final EventType INIT_CONFIG = new EventType(START_EVENT++);
    public static final EventType LOAD_PROJECT = new EventType(START_EVENT++);
    public static final EventType LOAD_EXPLORER_DATA = new EventType(START_EVENT++);
    public static final EventType INIT_SHEET = new EventType(START_EVENT++);
    public static final EventType CREATE_A_PERSON = new EventType(START_EVENT++);
    public static final EventType SHOW_NEW_PERSON_WINDOW = new EventType(START_EVENT++);
    public static final EventType CREATE_MENU_GROUP = new EventType(START_EVENT++);
    public static final EventType UPDATE_CELL_GROUP = new EventType(START_EVENT++);
    public static final EventType CREATE_CHECK_BOX = new EventType(START_EVENT++);
    public static final EventType LOAD_GROUPS = new EventType(START_EVENT++);
    public static final EventType RELATION_PERSON_GROUP_CLICKED = new EventType(START_EVENT++);
    public static final EventType PERSON_CREATED = new EventType(START_EVENT++);
    public static final EventType SHEET_DELETED = new EventType(START_EVENT++);
    public static final EventType DELETE_PERSON_CLICKED = new EventType(START_EVENT++);
    public static final EventType DELETE_PERSON_CONFIRMED = new EventType(START_EVENT++);
    public static final EventType PERSON_DELETED = new EventType(START_EVENT++);

    public static final EventType ABSENCE_GRID_CHANGED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_PERSONS_LOADED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_GRID_CREATED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_CREATE_ABSENCE_CLICKED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_CREATE_EVENT_CLICKED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_FILTER_CHANGED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_EVENTS_LOADED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_EVENT_SAVE_SUCCESS = new EventType(START_EVENT++);
    public static final EventType ABSENCE_ABSENCES_LOADED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_ABSENCE_SAVE_SUCCESS = new EventType(START_EVENT++);
    public static final EventType ABSENCE_EVENT_EDITOR_CLICKED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_ABSENCE_EDITOR_CLICKED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_GROUP_CHANGED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_REFRESH_CLICKED = new EventType(START_EVENT++);
    public static final EventType ABSENCE_ABSENCE_EDITOR_HIDE = new EventType(START_EVENT++);

    public static final EventType GROUP_PERSON_FILTER_CHANGED = new EventType(START_EVENT++);
    public static final EventType GROUP_GROUP_DROPPED = new EventType(START_EVENT++);
    public static final EventType GROUP_PERSON_MOVED = new EventType(START_EVENT++);
    public static final EventType GROUP_PERSON_DROPPED = new EventType(START_EVENT++);
    public static final EventType GROUP_FIRST_LOAD = new EventType(START_EVENT++);
    public static final EventType GROUP_GROUP_REFRESH_CLICKED = new EventType(START_EVENT++);
    public static final EventType GROUP_CREATE_GROUP_CLICKED = new EventType(START_EVENT++);
    public static final EventType GROUP_CREATE_GROUP_CONFIRMED = new EventType(START_EVENT++);
    public static final EventType GROUP_RENAME_GROUP_CLICKED = new EventType(START_EVENT++);
    public static final EventType GROUP_RENAME_GROUP_CONFIRMED = new EventType(START_EVENT++);
    public static final EventType GROUP_DELETE_GROUP_CLICKED = new EventType(START_EVENT++);
    public static final EventType GROUP_MAKE_LEADER_CLICKED = new EventType(START_EVENT++);
    public static final EventType GROUP_REMOVE_PERSON_CLICKED = new EventType(START_EVENT++);
    public static final EventType GROUP_PROJECT_REFRESH_CLICKED = new EventType(START_EVENT++);
    public static final EventType GROUP_REMOVE_FROM_PROJECT_CLICKED = new EventType(START_EVENT++);
    public static final EventType PERSON_SHEET_CHANGED = new EventType(START_EVENT++);
    
    public static final EventType CONTRACT_SHEET_CHANGED = new EventType(START_EVENT++);
    public static final EventType SHEET_DELETED_CONTRACT = new EventType(START_EVENT++);
    public static final EventType CONTRACT_CREATED = new EventType(START_EVENT++);
    public static final EventType NEW_CONTRACT_CLICKED = new EventType(START_EVENT++);
    public static final EventType NEW_CONTRACT_AMENDMENT_CLICKED = new EventType(START_EVENT++);
    public static final EventType CREATE_CONTRACT_REQUESTED = new EventType(START_EVENT++);
    public static final EventType RELATION_CONTRACT_GROUP_CLICKED = new EventType(START_EVENT++);
    public static final EventType DELETE_CONTRACT_CLICKED = new EventType(START_EVENT++);
    public static final EventType DELETE_CONTRACT_CONFIRMED = new EventType(START_EVENT++);
    public static final EventType CONTRACT_DELETED = new EventType(START_EVENT++);
    public static final EventType PERSON_TAB_CLICKED = new EventType(START_EVENT++);
    public static final EventType CONTRACT_TAB_CLICKED = new EventType(START_EVENT++);
    public static final EventType CONTRACT_PERSON_CHANGED = new EventType(START_EVENT++);
    public static final EventType CONTRACT_DELETE_CONFIRMED = new EventType(START_EVENT++);

}
