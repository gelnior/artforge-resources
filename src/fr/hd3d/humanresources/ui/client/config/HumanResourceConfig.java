package fr.hd3d.humanresources.ui.client.config;

/**
 * Constants needed by the application : image path, cookie variable name...
 * 
 * @author HD3D
 */
public class HumanResourceConfig
{
    /** Cookie field that contains last selected sheet ID. */
    public static final String COOKIE_SHEET_VALUE = "selected_resource_sheet";
    public static final String TAB_COOKIE_VAR = "teams-tab";

}
