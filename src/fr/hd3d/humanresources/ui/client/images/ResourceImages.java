package fr.hd3d.humanresources.ui.client.images;

import com.extjs.gxt.ui.client.util.IconHelper;
import com.google.gwt.user.client.ui.AbstractImagePrototype;


/**
 * ResourceImages provides easy access to icon displayed in Human Resources application.
 * 
 * @author HD3D
 */
public class ResourceImages
{

    /** Group link icon path. */
    public static final String PERSON_LINK_ICON_PATH = "images/group_link.png";
    /** Delete person icon path. */
    public static final String PERSON_DELETE_ICON_PATH = "images/person_delete.png";
    /** Person icon path. */
    public static final String PERSON_ICON_PATH = "images/person.png";
    /** Group icon path. */
    public static final String GROUP_ICON_PATH = "images/group.png";
    /** Leader icon path. */
    public static final String LEADER_ICON_PATH = "images/leader.png";

    public static final String CONTRACT_ICON_PATH = "images/shape_square_add.png";

    public static final String CONTRACT_AMENDMENT_ICON_PATH = "images/shape_square_add.png";

    public static final String CANCEL_ICON_PATH = "images/cancel.png";

    /**
     * @return Person icon with a tiny delete icon over.
     */
    public static AbstractImagePrototype getDeletePersonIcon()
    {
        return IconHelper.create(PERSON_DELETE_ICON_PATH);
    }

    /**
     * @return Person icon with a tiny link icon over.
     */
    public static AbstractImagePrototype getPersonLinkIcon()
    {
        return IconHelper.create(PERSON_LINK_ICON_PATH);
    }

    /**
     * @return Person icon.
     */
    public static AbstractImagePrototype getPersonIcon()
    {
        return IconHelper.create(PERSON_ICON_PATH);
    }

    /**
     * @return Group icon.
     */
    public static AbstractImagePrototype getGroupIcon()
    {
        return IconHelper.create(GROUP_ICON_PATH);
    }

    /**
     * @return Leader icon.
     */
    public static AbstractImagePrototype getLeaderIcon()
    {
        return IconHelper.createPath("images/leader.png");
    }

    /**
     * @return Leader icon.
     */
    public static AbstractImagePrototype getGroupAddIcon()
    {
        return IconHelper.createPath("images/group_add.png");
    }

    /**
     * @return Contract icon.
     */
    public static AbstractImagePrototype getContractIcon()
    {
        return IconHelper.createPath("images/shape_square_add.png");
    }

    /**
     * @return Contract Amendment icon.
     */
    public static AbstractImagePrototype getContractAmendmentIcon()
    {
        return IconHelper.createPath("images/shape_square_add.png");
    }

}
