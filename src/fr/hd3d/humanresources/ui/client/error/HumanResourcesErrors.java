package fr.hd3d.humanresources.ui.client.error;

/**
 * BreakdownErrors contains all error code values sent to the error dispatcher.
 * 
 * @author HD3D
 */
public class HumanResourcesErrors
{
    public static final int NO_SELECTION = 301;
    public static final int TOO_BIG_SELECTION = 302;
}
