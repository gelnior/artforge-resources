package fr.hd3d.humanresources.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:
 * 	'/home/frank.rousseau/workspace/HumanRessources/src/fr/hd3d/humanresources/ui/client/constant/HumanResourcesConstants.properties'.
 */
public interface HumanResourcesConstants extends com.google.gwt.i18n.client.Constants {
  
  /**
   * Translated "Absence".
   * 
   * @return translated "Absence"
   */
  @DefaultStringValue("Absence")
  @Key("Absence")
  String Absence();

  /**
   * Translated "Absence Editor".
   * 
   * @return translated "Absence Editor"
   */
  @DefaultStringValue("Absence Editor")
  @Key("AbsenceEditor")
  String AbsenceEditor();

  /**
   * Translated "Add new person".
   * 
   * @return translated "Add new person"
   */
  @DefaultStringValue("Add new person")
  @Key("AddNewPerson")
  String AddNewPerson();
  
  /**
   * Translated "Add new contract".
   * 
   * @return translated "Add new contract"
   */
  @DefaultStringValue("Add new contract")
  @Key("AddNewContract")
  String AddNewContract();
  
  /**
   * Translated "Add new contract".
   * 
   * @return translated "Add new contract amendment"
   */
  @DefaultStringValue("Add new contract amendment")
  @Key("AddNewContractAmendment")
  String AddNewContractAmendment();

  /**
   * Translated "Add person to groups".
   * 
   * @return translated "Add person to groups"
   */
  @DefaultStringValue("Add person to groups")
  @Key("AddPersonToGroups")
  String AddPersonToGroups();

  /**
   * Translated "Are you sure to delete selected persons?".
   * 
   * @return translated "Are you sure to delete selected persons?"
   */
  @DefaultStringValue("Are you sure to delete selected persons?")
  @Key("AreYouSureToDelete")
  String AreYouSureToDelete();

  /**
   * Translated "Create group".
   * 
   * @return translated "Create group"
   */
  @DefaultStringValue("Create group")
  @Key("CreateGroup")
  String CreateGroup();

  /**
   * Translated "Delete Group".
   * 
   * @return translated "Delete Group"
   */
  @DefaultStringValue("Delete Group")
  @Key("DeleteGroup")
  String DeleteGroup();

  /**
   * Translated "Delete selected people".
   * 
   * @return translated "Delete selected people"
   */
  @DefaultStringValue("Delete selected people")
  @Key("DeleteSelectedPeople")
  String DeleteSelectedPeople();
  
  /**
   * Translated "Delete selected contract".
   * 
   * @return translated "Delete selected contract"
   */
  @DefaultStringValue("Delete selected contract")
  @Key("DeleteSelectedContract")
  String DeleteSelectedContract();

  /**
   * Translated "Event Editor".
   * 
   * @return translated "Event Editor"
   */
  @DefaultStringValue("Event Editor")
  @Key("EventEditor")
  String EventEditor();

  /**
   * Translated "&nbsp;&nbsp;From: ".
   * 
   * @return translated "&nbsp;&nbsp;From: "
   */
  @DefaultStringValue("&nbsp;&nbsp;From: ")
  @Key("From")
  String From();

  /**
   * Translated "Group".
   * 
   * @return translated "Group"
   */
  @DefaultStringValue("Group")
  @Key("Group")
  String Group();

  /**
   * Translated "Make leader".
   * 
   * @return translated "Make leader"
   */
  @DefaultStringValue("Make leader")
  @Key("MakeLeader")
  String MakeLeader();

  /**
   * Translated "No record selected, please select a record first.".
   * 
   * @return translated "No record selected, please select a record first."
   */
  @DefaultStringValue("No record selected, please select a record first.")
  @Key("NoRecordSelected")
  String NoRecordSelected();

  /**
   * Translated "Please select only one record before editing absence.".
   * 
   * @return translated "Please select only one record before editing absence."
   */
  @DefaultStringValue("Please select only one record before editing absence.")
  @Key("OneRecordSelectedMax")
  String OneRecordSelectedMax();

  /**
   * Translated "Person".
   * 
   * @return translated "Person"
   */
  @DefaultStringValue("Person")
  @Key("Person")
  String Person();

  /**
   * Translated "Refresh Group".
   * 
   * @return translated "Refresh Group"
   */
  @DefaultStringValue("Refresh Group")
  @Key("RefreshGroup")
  String RefreshGroup();

  /**
   * Translated "Remove from Group".
   * 
   * @return translated "Remove from Group"
   */
  @DefaultStringValue("Remove from Group")
  @Key("RemoveFromGroup")
  String RemoveFromGroup();

  /**
   * Translated "Rename group".
   * 
   * @return translated "Rename group"
   */
  @DefaultStringValue("Rename group")
  @Key("RenameGroup")
  String RenameGroup();

  /**
   * Translated "Teams".
   * 
   * @return translated "Teams"
   */
  @DefaultStringValue("Teams")
  @Key("Teams")
  String Teams();

  /**
   * Translated "&nbsp;&nbsp;To:".
   * 
   * @return translated "&nbsp;&nbsp;To:"
   */
  @DefaultStringValue("&nbsp;&nbsp;To:")
  @Key("To")
  String To();

  /**
   * Translated "Type Work Last Name...".
   * 
   * @return translated "Type Work Last Name..."
   */
  @DefaultStringValue("Type Work Last Name...")
  @Key("TypeWorkerLastName")
  String TypeWorkerLastName();

  /**
   * Translated "Worker Search".
   * 
   * @return translated "Worker Search"
   */
  @DefaultStringValue("Worker Search")
  @Key("WorkerSearch")
  String WorkerSearch();


}
