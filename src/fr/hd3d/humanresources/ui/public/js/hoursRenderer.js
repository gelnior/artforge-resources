
{
	/**
	 * @param {Object} model
	 * @param {String} property
	 * @param {Object} css
	 */
    render:function(model, property, config)
    {
    	var milliseconds = model.get(property);
    	
    	var seconds = milliseconds / 1000;
    	
    	return seconds ;
    }
}
